<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

$addon_filemanager = bab_getAddonInfosInstance('filemanager');

if (!$addon_filemanager) {
    return;
}

define('FUNC_FILEMANAGER_PHP_PATH', $addon_filemanager->getPhpPath());
define('FUNC_FILEMANAGER_PORTLETS_PATH', FUNC_FILEMANAGER_PHP_PATH . 'portlets/');
bab_functionality::includeFile('App');




/**
 * Func_App_Filemanager
 */
class Func_App_Filemanager extends Func_App
{

    public function __construct()
    {
        parent::__construct();

        $this->addonPrefix = 'filemanager';
        $this->addonName = 'filemanager';
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/filemanager/main';
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'The Ovidentia File Manager';
    }







    function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translate = bab_functionality::get('Translate/Gettext');
        /* @var $translate Func_Translate_Gettext */
        $translate->setLanguage($language);
    }


    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    function translate($str, $str_plurals = null, $number = null)
    {
        require_once FUNC_FILEMANAGER_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('filemanager');
            $translation = $translate->translate($str, $str_plurals, $number);
        }
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }

        return $translation;
    }


    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        parent::includeController();
        require_once FUNC_FILEMANAGER_PHP_PATH . 'filemanager.ctrl.php';
    }

    /**
     *
     * @return filemanager_Controller
     */
    function Controller()
    {
        self::includeController();
        return new filemanager_Controller($this);
    }


    /**
     * Include class filemanager_Access
     */
    protected function includeAccess()
    {
        parent::includeAccess();
        require_once FUNC_FILEMANAGER_PHP_PATH . '/access.class.php';
    }



    /**
     * Include class filemanager_Ui
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once FUNC_FILEMANAGER_PHP_PATH . '/ui.class.php';
    }

    /**
     * The filemanager_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return filemanager_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('filemanager_Ui')->setApp($this);
    }


    /**
     * Get upload path
     * if the method return null, no upload functionality
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        return new bab_Path(bab_getAddonInfosInstance('filemanager')->getUploadPath());
    }


    /**
     * Include class app_Portlet
     *
     */
    protected function includePortlet()
    {
        parent::includePortlet();
        require_once FUNC_FILEMANAGER_PORTLETS_PATH . '/portlet.class.php';
    }



    /**
     * The app_Portlet object propose an access to all portlets
     *
     * @return filemanager_Portlet
     */
    public function Portlet()
    {
        $this->includePortlet();
        $portlet = bab_getInstance('filemanager_Portlet')->setApp($this);
        $portlet->includeBase();
        return $portlet;
    }


    /**
     * Returns the configuration for the specified path key
     *
     * @param string    $path           The full path with the key
     * @param mixed     $defaultValue
     * @return mixed
     */
    public function getConf($path, $defaultValue = null)
    {
        static $registry = null;
        if (!isset($registry)) {
            $registry = bab_getRegistry();
        }

        if (substr($path, 0, 1) !== '/') {
            $path = '/' . $path;
        }
        $path = '/' . $this->addonPrefix . $path;

        if (defined($path)) {
            return constant($path);
        }

        $elements = explode('/', $path);
        $key = array_pop($elements);
        $path = implode('/', $elements);
        $registry->changeDirectory($path);
        $value = $registry->getValue($key);

        if (!isset($value)) {
            $value = $defaultValue;
        }

        return $value;
    }

    /**
     * SET
     */

    //File Set
    public function includeFileSet()
    {
        require_once 'file.class.php';
    }

    public function FileClassName()
    {
        return 'filemanager_File';
    }

    public function FileSetClassName()
    {
        return $this->FileClassName() . 'Set';
    }

    /**
     *
     * @return filemanager_FileSet
     */
    public function FileSet()
    {
        $this->includeFileSet();
        $className = $this->FileSetClassName();
        $set = new $className($this);
        return $set;
    }

    //Folder Set
    public function FolderClassName()
    {
        return 'filemanager_Folder';
    }

    public function FolderSetClassName()
    {
        return $this->FolderClassName() . 'Set';
    }

    /**
     *
     * @return filemanager_FolderSet
     */
    public function FolderSet()
    {
        $this->includeFileSet();
        $className = $this->FolderSetClassName();
        $set = new $className($this);
        return $set;
    }

    //FileVersion Set
    public function FileVersionClassName()
    {
        return 'filemanager_FileVersion';
    }

    public function FileVersionSetClassName()
    {
        return $this->FileVersionClassName() . 'Set';
    }

    /**
     *
     * @return filemanager_FileVersionSet
     */
    public function FileVersionSet()
    {
        $this->includeFileSet();
        $className = $this->FileVersionSetClassName();
        $set = new $className($this);
        return $set;
    }

    //FileTagLinkSet
    public function FileTagLinkClassName()
    {
        return 'filemanager_FileTagLink';
    }

    public function FileTagLinkSetClassName()
    {
        return $this->FileTagLinkClassName() . 'Set';
    }

    /**
     *
     * @return filemanager_FileTagLinkSet
     */
    public function FileTagLinkSet()
    {
        $this->includeFileSet();
        $className = $this->FileTagLinkSetClassName();
        $set = new $className($this);
        return $set;
    }

}
