<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
require_once dirname(__FILE__) . '/file.php';


/**
 * An extension of the SplFileInfo corresponding to ovidentia's filemanager files.
 * Standard isWritable and isReadable file are mapped to corresponding ACL.
 *
 * For now only work for personal folders.
 *
 *
 * @since 7.0.0
 */
class filemanager_UserFileInfo extends bab_FileInfo
{
    /**
     * @var string
     */
    private $fmPathname = null;


    /**
     * {@inheritdoc}
     *
     * @see SplFileInfo::getFilename()
     */
    public function getFilename()
    {
        $return = parent::getFilename();
        if ($return == '') {
            $temp = new filemanager_UserFileInfo($this->getPath());
            $return = $temp->getFilename();
        }
        return $return;
    }



    /**
     * Returns the FM pathname i.e. "DG0/Folder1/SubFolder1/file.txt".
     *
     * @return string
     */
    public function getFmPathname()
    {
        if (!isset($this->fmPathname)) {
            $collectivePath = BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath;

            $fmPathname = substr($this->getPathname(), strlen($collectivePath));

            $fmPathname = BAB_PathUtil::sanitize($fmPathname);

            if (substr($fmPathname, 0, 1) == '/') {
                $fmPathname = substr($fmPathname, 1);
            }

            $this->fmPathname = $fmPathname;
        }
        return $this->fmPathname;
    }
}


class filemanager_FolderOrFile
{
    /**
     * Prefix to collective filemanager folders (usually followed by delegation id).
     * @var string
     */
    const delegationPrefix = 'DG';

    /**
     * Prefix to personal filemanager folders (usually followed by user id).
     * @var string
     */
    const userPrefix = 'U';

    /**
     * The path to filemanager folders relative to the ovidentia uploadPath.
     * @var string
     */
    const relativeFmRootPath = 'fileManager/';

    /**
     * The path to collective folders relative to the ovidentia uploadPath.
     * @var string
     */
    const relativeFmCollectivePath = 'fileManager/collectives/';

    /**
     * The path to personal folders relative to the ovidentia uploadPath.
     * @var string
     */
    const relativeFmPersonalPath = 'fileManager/users/';

    /**
     * The path to filemanager temp folders relative to the ovidentia uploadPath.
     * @var string
     */
    const relativeFmTempPath = 'fileManager/temp/';


    protected $path = null;

    public function __construct($path)
    {
        $this->path = $path;

        return $this;
    }

    public function getState()
    {
        return '';
    }

    public function getDescription()
    {
        return '';
    }

    public function getAuthorId()
    {
        return bab_getUserId();
    }

    public function getModifierId()
    {
        return bab_getUserId();
    }

    public function getCreationDate()
    {
        return '';
    }

    public function getModifiedDate()
    {
        return '';
    }

    public function getFullPathname()
    {
        $sFmPath = BAB_FmFolderHelper::getUploadPath() . self::relativeFmPersonalPath;
        return $sFmPath . $this->getPathName() . $this->getName();
    }

    public function getPathname()
    {
        $path = explode('/', $this->path);
        array_pop($path);
        array_shift($path);

        return filemanager_translate('user folder').'/'.implode('/',$path);
    }

    public function getName()
    {
        $path = explode('/', $this->path);
        $name = array_pop($path);
        return $name;
    }
}


class filemanager_FmFolderFile extends filemanager_FolderOrFile
{

}

class filemanager_FolderFile extends filemanager_FolderOrFile
{

}


/**
 * filemanager_FileSet
 *
 * @property ORM_PkField        $id
 * @property ORM_StringField    $name
 * @property ORM_StringField    $description
 * @property ORM_StringField    $path
 * @property ORM_IntField       $id_owner
 * @property ORM_StringField    $bgroup
 * @property ORM_IntField       $link
 * @property ORM_StringField    $readonly
 * @property ORM_StringField    $state
 * @property ORM_DatetimeField  $created
 * @property ORM_UserField      $author
 * @property ORM_DatetimeField  $modified
 * @property ORM_UserField      $modifiedby
 * @property ORM_StringField    $confirmed
 * @property ORM_IntField       $hits
 * @property ORM_IntField       $size
 * @property ORM_IntField       $max_downloads
 * @property ORM_IntField       $idfai
 * @property ORM_IntField       $edit
 * @property ORM_IntField       $ver_major
 * @property ORM_IntField       $ver_minor
 * @property ORM_StringField    $ver_comment
 * @property ORM_IntField       $index_status
 * @property ORM_IntField       $iIdDgOwner
 * @property ORM_IntField       $display_position
 *
 * @property ORM_StringField    $fmPrefix           The prefix of the file in the filemanager e.g. 'DGx' or 'Ux'
 * @property ORM_StringField    $fmPath             The path of the file with a trailing '/' in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/'
 * @property ORM_StringField    $fmPathname         The pathname of the file in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/file.txt'
 */
class filemanager_FileSet extends ORM_RecordSet
{
    /**
     * @var string
     */
    public $uploadPath;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTableName(BAB_FILES_TBL);
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('The file name'),
            ORM_StringField('description')
                ->setDescription('The file description'),
            ORM_StringField('path')
                ->setDescription('The file relative path'),
            ORM_IntField('id_owner')
                ->setDescription('The folder id in bab_fm_folders table the file is in'),
            ORM_StringField('bgroup', 1)
                ->setDescription('Y = file is in delegation, N = file is personal'),
            ORM_IntField('link'),
            ORM_StringField('readonly', 1)
                ->setDescription('Y = file is read only, N = file is not read only'),
            ORM_StringField('state', 1),
            ORM_DatetimeField('created'),
            ORM_UserField('author'),
            ORM_DatetimeField('modified'),
            ORM_UserField('modifiedby'),
            ORM_StringField('confirmed', 1),
            ORM_IntField('hits'),
            ORM_IntField('size'),
            ORM_IntField('max_downloads'),
            ORM_IntField('downloads'),
            ORM_IntField('idfai'),
            ORM_IntField('edit'),
            ORM_IntField('ver_major'),
            ORM_IntField('ver_minor'),
            ORM_StringField('ver_comment'),
            ORM_IntField('index_status'),
            ORM_IntField('iIdDgOwner'),
            ORM_IntField('display_position')
        );

        $this->uploadPath = BAB_FmFolderHelper::getUploadPath() . 'fileManager/';
    }

    /**
     * Adds and returns the field corresponding to the prefix of the file in the filemanager e.g. 'DGx' or 'Ux' or 'TRDGx' or 'TRUx'
     * @return ORM_StringField
     */
    public function fmPrefix()
    {
        if (!isset($this->fmPrefix)) {
            $this->addFields(
                $this->bgroup->ifEquals(
                    'Y',
                    $this->concat(
                        $this->state->ifEquals(
                            'D',
                            'TRDG',
                            'DG'
                        ),
                        $this->iIdDgOwner
                    ),
                    $this->concat(
                        $this->state->ifEquals(
                            'D',
                            'TRU',
                            'U'
                        ),
                        $this->id_owner
                    )
                )->setName('fmPrefix')
            );
        }
        return $this->fmPrefix;
    }

    /**
     * Adds and returns the field corresponding to the path of the file with a trailing '/' in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/'
     * @return ORM_StringField
     */
    public function fmPath()
    {
        if (!isset($this->fmPath)) {
            $this->addFields(
                $this->fmPrefix()->concat('/', $this->path)
                    ->setName('fmPath')
            );
        }
        return $this->fmPath;
    }

    /**
     * Adds and returns the field corresponding to the pathname of the file in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/file.txt'
     * @return ORM_StringField
     */
    public function fmPathname()
    {
        if (!isset($this->fmPathname)) {
            $this->addFields(
                $this->fmPath()->concat($this->name)
                    ->setName('fmPathname')
            );
        }
        return $this->fmPathname;
    }


    /**
     *
     * @param string $fmPathname         e.g. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return filemanager_File|null
     */
    public function getByFmPathname($fmPathname)
    {
        $pathElements = explode('/', $fmPathname);

        array_shift($pathElements);
        $fileName = array_pop($pathElements);

        $path = implode('/', $pathElements) . '/';

        $conditions = array(
            $this->name->is($fileName),
            $this->path->is($path)
        );
        if (filemanager_File::isPersonalPath($fmPathname)) {
            $conditions[] = $this->isPersonal();
            $conditions[] = $this->id_owner->is(filemanager_File::getPathOwnerId($fmPathname));
        } else {
            $conditions[] = $this->isCollective();
            $conditions[] = $this->iIdDgOwner->is(filemanager_File::getDelegationId($fmPathname));
        }

        return $this->get($this->all($conditions));
    }



    /**
     * @return ORM_IsCriterion
     */
    public function isReadonly()
    {
        return $this->readonly->is('Y');
    }

    /**
     * @return ORM_IsCriterion
     */
    public function isPersonal()
    {
        return $this->bgroup->is('N');
    }

    /**
     * @return ORM_IsCriterion
     */
    public function isCollective()
    {
        return $this->bgroup->is('Y');
    }

    /**
     * @return ORM_IsCriterion
     */
    public function isConfirmed()
    {
        return $this->confirmed->is('Y');
    }
}


/**
 * filemanager_File
 *
 * @property int        $id
 * @property string     $name
 * @property string     $description
 * @property string     $path
 * @property int        $id_owner
 * @property string     $bgroup
 * @property int        $link
 * @property string     $readonly
 * @property string     $state
 * @property string     $created
 * @property int        $author
 * @property string     $modified
 * @property int        $modifiedby
 * @property string     $confirmed
 * @property int        $hits
 * @property int        $size
 * @property int        $max_downloads
 * @property int        $idfai
 * @property int        $edit
 * @property int        $ver_major
 * @property int        $ver_minor
 * @property string     $ver_comment
 * @property int        $index_status
 * @property int        $iIdDgOwner
 * @property int        $display_position
 *
 * @property string     $fmPrefix           The prefix of the file in the filemanager e.g. 'DGx' or 'Ux'
 * @property string     $fmPath             The path of the file with a trailing '/' in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/'
 * @property string     $fmPathname         The pathname of the file in the filemanager e.g. 'DGx/Folder1/SubFolder1.1/file.txt'
 *
 * @method filemanager_FileSet getParentSet()
 */
class filemanager_File extends ORM_Record
{
    /**
     * @return bool
     */
    public function isReadonly()
    {
        return $this->readonly === 'Y';
    }

    /**
     * @return bool
     */
    public function isPersonal()
    {
        return $this->bgroup === 'N';
    }

    /**
     * @return bool
     */
    public function isCollective()
    {
        return $this->bgroup === 'Y';
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed === 'Y';
    }

    /**
     * @return bool
     */
    public function isTrashed()
    {
        return strpos($this->state, 'D') !== false;
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->edit != 0;
    }

    public function getReference()
    {
        return 'ovidentia:///files/file/'.$this->id;
    }

    public function lockedBy()
    {
        if($this->isLocked()){
            $App = filemanager_App();
            $set = $App->FileVersionSet();
            $version = $set->get(
                $set->id->is($this->edit)
                ->_AND_($set->confirmed->is('N'))
            );
            if(!$version){
                return null;
            }
            return $version->author;
        }
        return null;
    }

    /**
     * Whether or not a user can unlock a file, if $userId is null, then we check with the current user.
     * BEWARE : this method does not use the filemanager_canUpdateFileInFolder. It is strongly recommanded that you also use the filemanager_canUpdateFileInFolder function to check more rights
     * @param int $userId
     * @return boolean
     */
    public function canBeUnlockedByUser($userId = null)
    {
        if(!isset($userId)){
            $userId = bab_getUserId();
        }
        if($this->lockedBy() != $userId){
            if(bab_isMemberOfGroup(BAB_ADMINISTRATOR_GROUP, $userId)){
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->ver_major.'.'.$this->ver_minor;
    }

    /**
     * @return int
     */
    public function getModifierId()
    {
        return $this->modifiedby;
    }

    /**
     * @return string ISO Date
     */
    public function getCreationDate()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param bool $withTrash The trash is virtually handled, as the file is not put into a specific trash folder. If $withTrash is set to false, then the real full path to the file will be returned (if the file is a trash - this does nothing if the file is not trashed)
     * @return string
     */
    public function getFullPath($withTrash = true)
    {
        return $this->getFolderFullPath($withTrash) . $this->name;
    }

    /**
     * @param bool $withTrash The trash is virtually handled, as the file is not put into a specific trash folder. If $withTrash is set to false, then the real folder full path to the file will be returned (if the file is a trash - this does nothing if the file is not trashed)
     * @return string
     */
    public function getFolderFullPath($withTrash = true)
    {
        $set = $this->getParentSet();
        $fullPath = $set->uploadPath;
        $fullPath .= $this->isPersonal() ? 'users' : 'collectives';
        $fullPath .= '/' . $this->getFmPath($withTrash);
        return $fullPath;
    }

    /**
     * Return the fmPath of the file
     * 'DGx/folder1/folder2' ou 'Ux/...'
     * @param bool $withTrash The trash is virtually handled, as the file is not put into a specific trash folder. If $withTrash is set to false, then the real fm path to the file will be returned (if the file is a trash - this does nothing if the file is not trashed)
     * @return string
     */
    public function getFmPath($withTrash = true)
    {
        $fmPath = $this->isPersonal() ? 'U' . $this->id_owner : 'DG' . $this->iIdDgOwner;
        if($this->isTrashed() && $withTrash){
            $fmPath = 'TR'.$fmPath;
        }
        $fmPath .= '/' . $this->path;
        return $fmPath;
    }

    /**
     * Return the fmPathName of the file
     * 'DGx/folder1/folder2/file.txt' ou 'Ux/.../file.txt'
     * @param bool $withTrash The trash is virtually handled, as the file is not put into a specific trash folder. If $withTrash is set to false, then the real fm pathname to the file will be returned (if the file is a trash - this does nothing if the file is not trashed)
     * @return string
     */
    public function getFmPathName($withTrash = true)
    {
        return $this->getFmPath($withTrash).$this->name;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->author;
    }

    public function getAuthorName()
    {
        return bab_getUserName($this->author);
    }

    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string ISO Date
     */
    public function getModifiedDate()
    {
        return $this->modified;
    }

    /**
     * @param boolean $unity If set to true, the size + unit will be return (string), otherwise only the size will be returned (int)
     * @return int|string
     */
    public function getSize($unity = true)
    {
        if(!$unity){
            return $this->size;
        }

        if ($this->size > 1024*1024) {
            $sizeText = round($this->size / (1024*1024), 1) . ' ' . filemanager_translate('MB');
        } elseif ($this->size > 1024) {
            $sizeText = round($this->size / 1024) . ' ' . filemanager_translate('KB');
        } else {
            $sizeText = $this->size . ' ' . filemanager_translate('Bytes');
        }

        return $sizeText;
    }


    /**
     *
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return number
     */
    public static function getPathOwnerId($path)
    {
        static $set = null;
        if (!isset($set)) {
            $App = filemanager_App();
            $set = $App->FileSet();
        }
        $set->fmPath();
        if (self::isPersonalPath($path)) {
            return self::getUserId($path);
        }
        else {
            $path = rtrim($path, '/').'/'; //We ensure the path has a trailing slash
            $record = $set->get($set->fmPath->is($path));
            if($record){
                /* @var $record filemanager_File */
                return $record->id_owner;
            }
        }
        return null;
    }


    /**
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return number
     */
    public static function getDelegationId($path)
    {
        $pathElements = explode('/', $path);
        $dg = array_shift($pathElements);
        if(self::isTrashPath($path)){
            $dg = str_replace('TRDG', '', $dg);
        }
        else{
            $dg = str_replace('DG', '', $dg);
        }

        return (int)$dg;
    }

    /**
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return number
     */
    public static function getUserId($path)
    {
        $pathElements = explode('/', $path);
        $user = array_shift($pathElements);
        if(self::isTrashPath($path)){
            $user = str_replace('TRU', '', $user);
        }
        else{
            $user = str_replace('U', '', $user);
        }

        return (int)$user;
    }

    /**
     *
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return boolean
     */
    public static function isPersonalPath($path)
    {
        if(self::isTrashPath($path)){
            if (substr($path, 2, 1) === 'U') {
                return true;
            }
        }
        else if (substr($path, 0, 1) === 'U') {
            return true;
        }
        return false;
    }

    public static function isTrashPath($path)
    {
        if (substr($path, 0, 2) === 'TR') {
            return true;
        }
        return false;
    }


    /**
     *
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     *
     * @return string   .../upload/fileManager/collectives or .../upload/fileManager/users
     */
    public static function getBasePath($path)
    {
        static $set = null;
        if (!isset($set)) {
            $App = filemanager_App();
            $set = $App->FileSet();
        }
        $basePath = $set->uploadPath;
        $basePath .= self::isPersonalPath($path) ? 'users' : 'collectives';

        return $basePath;
    }

    /**
     *
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @return string 'Ux' or 'DGx'
     */
    public static function getRootPath($path)
    {
        $rootPath = self::isTrashPath($path) ? 'TR' : '';
        $rootPath .= self::isPersonalPath($path) ? 'U'.self::getPathOwnerId($path) : 'DG'.self::getDelegationId($path);
        return $rootPath;
    }


    /**
     * Check if the path given in parameter is linked to a folder.
     * Returns true if it is a folder, false if it is a file or does not exists or is empty
     * @param string $path           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     *
     * @return bool
     */
    public static function isFolder($path)
    {
        static $set = null;
        if (!isset($set)) {
            $App = filemanager_App();
            $set = $App->FileSet();
        }
        $set->fmPath();
        $folders = $set->select($set->fmPath->is($path));
        return $folders->count() > 0;
    }


    /**
     * @return bab_FileInfo
     */
    public function getFileInfo()
    {
        return new bab_FileInfo($this->getFullPath());
    }


    /**
     *
     * @param string $destination           Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @throws bab_FmFileErrorException
     * @return bool
     */
    public function unzip($destination)
    {
        $fileInfo = $this->getFileInfo();
        $fileFullPath = $this->getFullPath();

        if ($fileInfo->getExtension() != 'zip') {
            throw new bab_FmFileErrorException('Request unzip method on a file without zip extension');
        }
        if (filesize($fileFullPath) > $GLOBALS['babMaxZipSize']) {
            throw new bab_FmFileErrorException('The ZIP file size exceed the limit configured for the file manager');
        }

        $fileManagerEnv = & getEnvObject();

        /* @var $Zip Func_Archive_Zip */
        $Zip = bab_functionality::get('Archive/Zip');
        $tempPath = new bab_Path($GLOBALS['babUploadPath'], 'tmp', session_id());
        if ($tempPath->isDir()) {
            $tempPath->deleteDir();
        }
        $tempPath->createDir();

        $Zip->open($fileFullPath);
        $Zip->extractTo($tempPath->tostring());
        $Zip->close();

        if ($tempPath->isDir()) {
            $unzipSize = getDirSize($tempPath->tostring());
            if ($unzipSize + $fileManagerEnv->getFMTotalSize() > $GLOBALS['babMaxTotalSize']) {
                throw new bab_FmFileErrorException('The file size exceed the limit configured for the file manager');
            }
            return self::moveUnzipFolder($tempPath, $destination);
        }
    }



    public function unzipInSameDirectory()
    {
        return $this->unzip($this->getFmPath());
    }


    /**
     * Move the file unziped in $source into the same folder the file is in
     * DO NOT CALL THIS METHOD OUTSIDE THE unzipInSameDirectory() METHOD
     *
     * @param bab_Path $source
     * @param string $destination
     *            Eg. 'DGx/folder1/folder2' ou 'Ux/...'
     * @param string $absolutePath
     * @throws bab_FmFileErrorException
     * @return bool
     */
    private static function moveUnzipFolder(bab_Path $source, $destination)
    {
        $return = true;

        $absolutePath = self::getBasePath($destination);

        foreach ($source as $babPath) {
            if ($babPath->isDir()) {
                $currentBabPath = new bab_Path($absolutePath, $destination, $babPath->getBasename());
                $currentBabPath->createDir();
                $currentBabPath = new bab_Path($destination, $babPath->getBasename());

                $return = $return && self::moveUnzipFolder($babPath, $currentBabPath->tostring());
            } else {
                if (bab_charset::getDatabase() == 'utf8') {
                    if (! mb_check_encoding($babPath->getBasename(), 'UTF-8')) {
                        throw new bab_FmFileErrorException(sprintf('Can not unzip file: %s', mb_convert_encoding($babPath->getBasename(), 'UTF-8')));
                    }
                }
                $fmFile = bab_FmFile::move($babPath->tostring());
//                $currentBabPath = new bab_Path($destination, $babPath->getBasename());
                $return = $return && filemanager_importFmFile($fmFile, $destination);
            }
        }

        return $return;
    }

    /**
     *
     * @return filemanager_Folder|NULL
     */
    public function getFolderInfos()
    {
        if ($this->isCollective()) {
            $app = filemanager_App();
            $set = $app->FolderSet();
            return $set->get($set->id->is($this->id_owner));
        }
        return null;
    }

    /**
     *
     * @return bool
     */
    public function canDelete()
    {
        return filemanager_canDeleteFileInFolder($this->getFmPath(), $this->isPersonal());
    }

    /**
     *
     * @return bool
     */
    public function canDownload()
    {
        return filemanager_canDownloadFileFromFolder($this->getFmPath(), $this->isPersonal());
    }

    /**
     *
     * @return bool
     */
    public function canRename()
    {
        return $this->canBeUpdated();
    }

    /**
     * Checks whether the current user can replace the file.
     * Uses filemanager_canUpdateFileInFolder() method
     * @return boolean
     */
    public function canBeUpdated()
    {
        return filemanager_canUpdateFileInFolder($this->getFmPath(), $this->isPersonal());
    }

    /**
     * When restoring a file, checks if there is not arleady a file in the destination with the same name
     * @return bool
     */
    public function canBeRestored()
    {
        if(!$this->isTrashed()){
            return false;
        }
        $currentPath = $this->getFullPath(false);
        $dirname = dirname($currentPath);
        $basename = substr(basename($currentPath),1); //Remove the first char. On trashed files, the first char is a dot
        $newPath = $dirname . '/' . $basename;
        if(file_exists($newPath)){
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function restore()
    {
        if(!$this->isTrashed() || !$this->canBeRestored()){
            return false;
        }
        //Check rights
        $canRestore = false;
        if($this->isCollective()){
            $bManager	= bab_isAccessValid(BAB_FMMANAGERS_GROUPS_TBL, $this->id_owner);
            $bUpdate	= bab_isAccessValid(BAB_FMUPDATE_GROUPS_TBL, $this->id_owner);
            $canRestore = $bManager || $bUpdate;
        }
        else{
            $canRestore = $this->id_owner == bab_getUserId() || bab_isUserAdministrator();
        }
        if($canRestore){
            $fileHand = fopen($this->getFmPathName(), 'r');
            fclose($fileHand);

            $oldPathName = $this->getFullPath(false);
            $dirname = dirname($oldPathName);
            $basename = substr(basename($oldPathName),1); //Remove the first char. On trashed files, the first char is a dot
            $newPathname = $dirname . '/' . $basename;

            if(rename($oldPathName, $newPathname)){
                $this->state = str_replace('D', '', $this->state); //Remove deleted state
                $this->name = $basename;
                return $this->save();
            }
        }
        return false;
    }

    /**
     *
     * @return filemanager_FileVersion[]
     */
    public function getAllVersions()
    {
        $App = filemanager_App();
        $set = $App->FileVersionSet();

        $fmVers = $set->select($set->id_file->is($this->id))->orderDesc($set->date);
        return $fmVers;
    }

    public function getThesaurusTags()
    {
        $App = filemanager_App();
        $set = $App->FileTagLinkSet();

        $thesaurusTags = array();
        $fmTags = $set->select($set->reference->is($this->getReference()));
        foreach ($fmTags as $tag){
            $thesaurusTags[$tag->id_tag] = filemanager_getThesaurusTagNameById($tag->id_tag);
        }
        return $thesaurusTags;
    }

    /**
     * Set the file tags
     * @param array $tags Array of thesaurus tags id
     * @return bool True or False wether the set was successfull or not
     */
    public function setThesaurusTags($tags = array())
    {
        //No tag associated to the file, we need to remove all tags
        if(count($tags) < 1){
            $this->removeAllTags();
            return true;
        }
        //Check if the tags given are correct
        foreach ($tags as $tagId){
            $tagName = filemanager_getThesaurusTagNameById($tagId);
            if(!isset($tagName) || !is_string($tagName)){
                //At least one tag was not found
                return false;
            }
        }
        //All the tags given are correct, remove all tags and add the given ones (to avoid duplicates)
        $App = filemanager_App();
        $set = $App->FileTagLinkSet();
        $this->removeAllTags();
        foreach ($tags as $tagId){
            $newTag = $set->newRecord();
            $newTag->id_tag = $tagId;
            $newTag->setFile($this->id);
            $newTag->save();
        }
        return true;
    }

    /**
     * Remove all the tags associated to the file
     */
    public function removeAllTags()
    {
        $App = filemanager_App();
        $set = $App->FileTagLinkSet();

        //Select tags associated to the file
        $tags = $set->select($set->reference->is($this->getReference()));
        //Delete them
        foreach($tags as $tag){
            /* @var $tag filemanager_FileTagLink */
            $tag->delete();
        }
        return true;
    }

    /**
     * Delete the file.
     * If $withVersion is set to true, it will also delete every version of the file. This only works if the file is in a collective folder
     * @param bool $withVersion
     * @return bool
     */
    public function deleteFile($withVersion = false)
    {
        $pathname = $this->getFmPathName(false);
        if($this->isPersonal()){
            //Personal folders do not have versions, $withVersion is useless here
            return filemanager_personalFileDelete($pathname);
        }

        //First, we try do delete every versions of the file
        if($withVersion){
            $versions = $this->getAllVersions();
            if(count($versions) > 0){
                //File has versions
                $versionFolder = dirname($this->getFullPath(false)).'/OVF/';
                foreach ($versions as $version){
                    $filename = $version->getFilename();
                    if(file_exists($versionFolder.$filename)){
                        unlink($versionFolder.$filename);
                    }
                    $version->delete();
                }
            }
        }
        //Delete the file
        $directory = new bab_Directory();
        return $directory->deleteFile($this->getFmPathName(false));
    }
}

/**
 * filemanager_FolderSet
 *
 * @property ORM_PkField        $id
 * @property ORM_StringField    $folder
 * @property ORM_StringField    $sRelativePath
 * @property ORM_IntField       $manager
 * @property ORM_IntField       $idsa
 * @property ORM_StringField    $filenotify
 * @property ORM_StringField    $active
 * @property ORM_StringField    $version
 * @property ORM_IntField       $id_dgowner
 * @property ORM_StringField    $bhide
 * @property ORM_StringField    $auto_approbation
 * @property ORM_StringField    $baddtags
 * @property ORM_StringField    $bcap_downloads
 * @property ORM_IntField       $max_downloads
 * @property ORM_StringField    $bdownload_history
 * @property ORM_IntField       $manual_order
 */
class filemanager_FolderSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
        $this->setTableName(BAB_FM_FOLDERS_TBL);
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('folder')
                ->setDescription('The folder name'),
            ORM_StringField('sRelativePath')
                ->setDescription('The folder relative path'),
            ORM_IntField('manager'),
            ORM_IntField('idsa'),
            ORM_StringField('filenotify', 1),
            ORM_StringField('active', 1),
            ORM_StringField('version', 1),
            ORM_IntField('id_dgowner'),
            ORM_StringField('bhide', 1),
            ORM_StringField('auto_approbation', 1),
            ORM_StringField('baddtags', 1),
            ORM_StringField('bcap_downloads', 1),
            ORM_IntField('max_downloads'),
            ORM_StringField('bdownload_history', 1),
            ORM_IntField('manual_order')
        );
    }
}

/**
 * filemanager_Folder
 *
 * @property int       $id
 * @property string    $folder
 * @property string    $sRelativePath
 * @property int       $manager
 * @property int       $idsa
 * @property string    $filenotify
 * @property string    $active
 * @property string    $version
 * @property int       $id_dgowner
 * @property string    $bhide
 * @property string    $auto_approbation
 * @property string    $baddtags
 * @property string    $bcap_downloads
 * @property int       $max_downloads
 * @property string    $bdownload_history
 * @property int       $manual_order
 */
class filemanager_Folder extends ORM_Record
{

    /**
     *
     * @return bool
     */
    public function hasNotifications()
    {
        return $this->filenotify == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isVersioned()
    {
        return $this->version == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isHidden()
    {
        return $this->bhide == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isAutoApproved()
    {
        return $this->auto_approbation == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isThesaurusEnabled()
    {
        return $this->baddtags == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function hasMaxDownloads()
    {
        return $this->bcap_downloads == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function hasDownloadHistory()
    {
        return $this->bdownload_history == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function hasManualOrder()
    {
        return $this->manual_order == 1;
    }
}

/**
 * filemanager_FileVersionSet
 *
 * @property ORM_PkField            $id
 * @property ORM_IntField           $id_file
 * @property ORM_DatetimeField      $date
 * @property ORM_IntField           $author
 * @property ORM_IntField           $ver_major
 * @property ORM_IntField           $ver_minor
 * @property ORM_StringField        $comment
 * @property ORM_IntField           $idfai
 * @property ORM_StringField        $confirmed
 * @property ORM_IntField           $index_status
 */
class filemanager_FileVersionSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
        $this->setTableName(BAB_FM_FILESVER_TBL);
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('id_file'),
            ORM_DatetimeField('date'),
            ORM_IntField('author'),
            ORM_IntField('ver_major'),
            ORM_IntField('ver_minor'),
            ORM_StringField('comment'),
            ORM_IntField('idfai'),
            ORM_StringField('confirmed', 1),
            ORM_IntField('index_status')
        );
    }
}

/**
 * filemanager_Folder
 *
 * @property int       $id
 * @property int       $id_file
 * @property string    $date
 * @property int       $author
 * @property int       $ver_major
 * @property int       $ver_minor
 * @property string    $comment
 * @property int       $idfai
 * @property string    $confirmed
 * @property int       $index_status
 */
class filemanager_FileVersion extends ORM_Record
{
    /**
     *
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed == 'Y';
    }

    /**
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->ver_major.'.'.$this->ver_minor;
    }

    /**
     *
     * @return filemanager_File|NULL
     */
    public function getFmFile()
    {
        $App = filemanager_App();
        $set = $App->FileSet();

        return $set->get($set->id->is($this->id_file));
    }

    /**
     *
     * @return string
     */
    public function getAuthorName()
    {
        return bab_getUserName($this->author);
    }
    public function getFileName()
    {
        $file = $this->getFmFile();
        return $this->ver_major.','.$this->ver_minor.','.$file->name;
    }
}

/**
 * filemanager_FileTagLinkSet
 *
 * @property ORM_IntField           $id
 * @property ORM_IntField           $id_tag
 * @property ORM_StringField        $reference
 */
class filemanager_FileTagLinkSet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
        $this->setTableName('bab_tags_references');
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('id_tag'),
            ORM_StringField('reference')
        );
    }
}

/**
 * filemanager_FileTagLink
 *
 * @property ORM_IntField           $id
 * @property ORM_IntField           $id_tag
 * @property ORM_StringField        $reference
 */
class filemanager_FileTagLink extends ORM_Record
{
    public function setFile($fmFileId)
    {
        $this->reference = 'ovidentia:///files/file/'.$fmFileId;
        return $this;
    }
}