<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

/**
 * This exception is thrown when the system tries to perform
 * an operation on an object that the user is not allowed to.
 */
class filemanager_AccessException extends Exception
{
}



/**
 * Instanciates the App factory.
 *
 * @return Func_App_Filemanager
 */
function filemanager_App()
{
    return bab_Functionality::get('App/Filemanager');
}



/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function filemanager_translate($str, $str_plurals = null, $number = null)
{


    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('filemanager');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}

/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function filemanager_translateArray($arr)
{
    $newarr = $arr;

    foreach ($newarr as &$str) {
        $str = filemanager_translate($str);
    }
    return $newarr;
}


/**
 * @return filemanager_Controller
 */
function filemanager_Controller()
{
    return filemanager_App()->Controller();
}


/**
 * Instanciates the thumbnailer.
 *
 * @return Func_Thumbnailer
 */
function filemanager_Thumbnailer()
{
    $T = bab_functionality::get('Thumbnailer');
    return $T;
}





/**
 * Creates a folder in the addon's upload folder.
 *
 * @param string	$pathname	The full pathname of the new folder.
 * @return bool
 */
function filemanager_createFolder($pathname)
{
    if (strncmp($pathname, $GLOBALS['babAddonUpload'], strlen($GLOBALS['babAddonUpload']))) {
        return false;
    }
    $folder = $GLOBALS['babAddonUpload'];
    if (!is_dir($folder)) {
        if (!bab_mkdir($folder)) {
            return false;
        }
    }
    $relativePath = spaces_path(substr($pathname, strlen($GLOBALS['babAddonUpload'])));
    $subfolders = explode('/', $relativePath);

    foreach ($subfolders as $subfolder) {
        $folder .= $subfolder . '/';
        if (!is_dir($folder)) {
            if (!bab_mkdir($folder)) {
                return false;
            }
        }
    }
    return true;
}





/**
 * Concats all strings in the array $subPaths to form a path.
 * @see filemanager_path
 *
 * @param array $subPaths	An array of string
 */
function filemanager_concatPaths($subPaths)
{
    $isRelativePath = (substr($subPaths[0], 0, 1) !== '/');

    $allElements = array();
    foreach ($subPaths as $subPath) {
        $elements = explode('/', $subPath);
        foreach ($elements as $element) {
            if ($element === '..' && count($allElements) > 0) {
                array_pop($allElements);
            } elseif ($element !== '.' && $element !== '..' && $element !== '') {
                array_push($allElements, $element);
            }
        }
    }

    $path = implode('/', $allElements);
    if (!$isRelativePath) {
        $path = '/' . $path;
    }

    return $path;
}



/**
 * Concats all strings to form a path.
 * Eg. filemanager_path('a/b', 'c', 'd/e/', 'f/') => 'a/b/c/d/e/f'
 *
 * @param	string	$subPath...		On or more sub paths.
 * @return	string
 */
function filemanager_path($subPath)
{
    $subPaths = func_get_args();

    $path = filemanager_concatPaths($subPaths);

    return $path;
}



/**
 *
 * @param string $path
 * @throws Exception
 * @return string|mixed
 */
function filemanager_sanitizePath($path = null)
{
    if (!isset($path) || empty($path)) {
        $pathElements = array('DG0');
    } else if (substr($path, 0, 2) === 'DG') {
        $pathElements = explode('/', $path);
    } else if (substr($path, 0, 2) === 'TR') { 
        //TRASH
        $pathElements = explode('/', $path);
    }
    else {
        throw new Exception('Wrong path');
    }

    $delegationPrefix = array_shift($pathElements);

    $path = $delegationPrefix;

    if (!empty($pathElements)) {
        $path .= '/' . implode('/', $pathElements);
    }

    return $path;
}



/**
 * Returns the delegation id corresponding to the specified path.
 * @param string $path
 *
 * @return int 		The delegation id or null on error.
 */
function filemanager_getDelegationFromPath($path)
{
    if (substr($path, 0, 2) == 'TR') {
        $path = trim($path, 'TR');
    }
    if (substr($path, 0, 2) !== 'DG') {
        return null;
    }
    $dgend = strpos($path, '/', 2);
    $delegationId = (int)(substr($path, 2, $dgend - 2));
    return $delegationId;
}



/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function filemanager_redirect($url)
{
    global $babBody;

    if ($url instanceof Widget_Action) {
        $url = $url->url();
    }
    if (!empty($babBody->msgerror)) {
        $url .=  '&msgerror=' . urlencode($babBody->msgerror);
    }

    header('Location: ' . $url);
    die;
}


/**
 * @return Widget_FlowLayout
 */
function filemanager_Toolbar()
{
    $W = bab_Widgets();

    $toolbar = $W->FlowItems()
        ->addClass('widget-toolbar', Func_Icons::ICON_LEFT_16);

    return $toolbar;
}




/**
 * Create a bab_searchCriteria from a string.
 *
 * @param	bab_searchTestable	$testable
 * @param	string				$search
 * @return bab_searchCriteria
 */
function filemanager_searchStringToCriteria($testable, $search)
{
    $criteria = new bab_SearchInvariant;

    if (preg_match_all('/(?:([^"][^\s]+)|(?:"([^"]+)")|(\w))\s*/', $search, $matchs)) {

        $arr = array();

        foreach($matchs[1] as $key => $match) {
            if (trim($match)) {
                $arr[] = trim($match);
            }
            if (trim($matchs[2][$key])) {
                $arr[] = trim($matchs[2][$key]);
            }

            if (trim($matchs[3][$key])) {
                $arr[] = trim($matchs[3][$key]);
            }
        }

        foreach($arr as $keyword) {

            $keyword = trim($keyword, ' ,;.');

            if ($keyword) {
                $criteria = $criteria->_AND_($testable->contain($keyword));
            }
        }

        return $criteria;
    }

    return null;
}


function filemanager_getConf($key, $defaultValue = null)
{
    if (isset($_SESSION['filemanager_' . $key])) {
        return $_SESSION['filemanager_' . $key];
    }
    return null;
}

function filemanager_setConf($key, $value = null)
{
    $_SESSION['filemanager_' . $key] = $value;
}

function filemanager_zipFolderFile(bab_Path $source, $Zip, $currentPath = '', $notApproveFile, $gr = 'N'){
    foreach($source as $babPath){
        if($currentPath == ''){
            $currentBabPath = new bab_Path($babPath->getBasename());
        }else{
            $currentBabPath = new bab_Path($currentPath, $babPath->getBasename());
        }
        if($babPath->isDir()){
            filemanager_zipFolderFile($babPath, $Zip, $currentBabPath->tostring(), $notApproveFile, $gr);
        }else{
            if($gr == 'Y'){
                $folder = new bab_fileInfo($babPath->tostring());
                if(!$folder->isReadable()){
                    //$babBody->addError(bab_translate("Invalid directory"));
                    //return;
                    continue;
                }
            }
            if(in_array(realpath($babPath->tostring()), $notApproveFile)){
                continue;
            }
            $Zip->addFile($babPath->tostring(), $currentBabPath->tostring());

        }
    }
}



/**
 * Import a file into the file manager
 * if the file exists, the file is updated or a new version of the file is created
 * @param 	object	$fmFile			bab_fmFile instance
 * @param	string	$path
 *
 * @return 	bool
 */
function filemanager_importFmFile($fmFile, $path)
{
    include_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

    $gr = !filemanager_File::isPersonalPath($path);

    $id_owner = filemanager_File::getPathOwnerId($path);

    $pathElements = explode('/', $path);
    array_shift($pathElements);
    $sPathName = implode('/', $pathElements) . '/';

    $oFileManagerEnv = & getEnvObject();

    $oFileManagerEnv->sPath = (string) bab_convertToDatabaseEncoding(removeEndSlashes($sPathName));
    $oFileManagerEnv->sGr = $gr ? 'Y' : 'N';

    $oFileManagerEnv->iIdObject = $id_owner;

    $oFileManagerEnv->init();

    $sFullPathNane = filemanager_File::getBasePath($path) . '/' . $path . '/' . $fmFile->filename;

    if (!file_exists($sFullPathNane)) {
        // create new file
        return saveFile(array($fmFile), $id_owner, $gr ? 'Y' : 'N', $sPathName, array('0' => ''), '', 'N', null);
    }

    $oFolderFileSet = new BAB_FolderFileSet();

    $oPathName = & $oFolderFileSet->aField['sPathName'];
    $oName = & $oFolderFileSet->aField['sName'];
    $oIdOwner = & $oFolderFileSet->aField['iIdOwner'];
    $oGroup = & $oFolderFileSet->aField['sGroup'];
    $oIdDgOwner = & $oFolderFileSet->aField['iIdDgOwner'];

    $oCriteria = $oPathName->in($sPathName);
    $oCriteria = $oCriteria->_and($oName->in($fmFile->filename));
    $oCriteria = $oCriteria->_and($oIdOwner->in($id_owner));
    $oCriteria = $oCriteria->_and($oGroup->in($gr ? 'Y' : 'N'));
    $oCriteria = $oCriteria->_and($oIdDgOwner->in(bab_getCurrentUserDelegation()));

    $oFolderFile = $oFolderFileSet->get($oCriteria);

    if (! is_null($oFolderFile)) {
        $fm_file = fm_getFileAccess($oFolderFile->getId());
        $oFmFolder = & $fm_file['oFmFolder'];

        if (! $fm_file['bupdate']) {
            return false;
        }

        if ($gr && $oFmFolder->getVersioning() === 'Y') {
            // add a version
            fm_lockFile($oFolderFile->getId(), '');
            return fm_commitFile($oFolderFile->getId(), '', 'N', $fmFile);
        }

        // update a file
        return saveUpdateFile($oFolderFile->getId(), $fmFile, $fmFile->filename, $oFolderFile->getDescription(), '', $oFolderFile->getReadOnly(), 'Y', false, false, 0);
    }
    return false;
}


bab_Functionality::get('Icons');


