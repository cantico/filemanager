;<?php/*

[general]
name="filemanager"
version="1.1.82"
addon_type="EXTENSION"
encoding="ISO-8859-15"
mysql_character_set_database="latin1,utf8"
description="File manager"
description.fr="Module permettant d'afficher le gestionnaire de fichiers dans un portlet"
long_description="README.md"
long_description.en="README.en.md"
long_description.fr="README.fr.md"
delete=1
ov_version="8.6.97"
php_version="5.4.0"
addon_access_control="1"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="icon48.png"
image="thumbnail.png"
license="GPL-2.0+"
tags="extension,portlet"

[addons]
libapp              ="0.0.1"
widgets             ="1.1.81"
jquery              ="1.11.1.3"
LibFileManagement   ="0.2.14"
portlets            ="0.3"
LibTranslate        =">=1.12.0rc3.01"
LibOrm				="0.11.10"



[functionalities]
Thumbnailer="Available"
jquery="Available"

; */?>