<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

/**
 *
 */
class Func_WorkspaceAddon_FileManager extends Func_WorkspaceAddon implements workspace_Configurable
{
    public function getDescription()
    {
        return filemanager_translate('Enable the FileManager addon into the workspaces');
    }
    
    public function getName()
    {
        return filemanager_translate('File manager');
    }
    
    public function getConfigurationEditor($workspaceId)
    {
        $W = bab_Widgets();

        $editor = parent::getConfigurationEditor($workspaceId);
        $editor->addItem(
            $W->LabelledWidget(
                filemanager_translate('Activate file versioning'), 
                $W->CheckBox()->setName('isVersionEnabled')             
            )
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                filemanager_translate('Notify workspace members when a file is uploaded'),
                $W->CheckBox()->setName('notifyMembers')
            )
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                filemanager_translate('Maximum capacity in MB'),
                $W->NumberEdit(),
                'maxCapacity',
                filemanager_translate('0 to have no limit')
            )
        );

        return $editor;
    }
    
    public function getIconClassName()
    {
        return Func_Icons::APPS_FILE_MANAGER;
    }
    
    public function applyConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if($workspace->delegation == 0 || $workspace->name == ''){
                return;
            }
            $configuration = $this->getConfiguration($workspace->id);
            
            $fileApi = filemanager_App();
            $folderSet = $fileApi->FolderSet();
            
            /* @var $workspaceBaseFolder filemanager_Folder */
            $workspaceBaseFolder = $folderSet->get($folderSet->id_dgowner->is($workspace->delegation));
            if($workspaceBaseFolder){
                $workspaceBaseFolder->version = isset($configuration['isVersionEnabled']) ? ($configuration['isVersionEnabled'] == 1 ? 'Y' : 'N') :'N';
                $workspaceBaseFolder->filenotify = isset($configuration['notifyMembers']) ? ($configuration['notifyMembers'] == 1 ? 'Y' : 'N') :'N';
                $workspaceBaseFolder->save();
            }
            else{
                //The specified workspace does not have any filemanager created. We need to create one
                $this->initializeFileManager($workspace, $configuration);
            }
            /* @TODO Check right on filemanager to ensure the group from workspace is still the same for filemanager (what happens if we change the group from workspace edit ?) */
        }
    }
    
    public function getPortletDefinitionId()
    {
        /* @var $portletFunc Func_PortletBackend_FileManager */
        $portletFunc = bab_Functionality::get('PortletBackend/FileManager');
        if($portletFunc){
            /* @var $portlet filemanager_PortletDefinition_FileManager */
            $portlet = $portletFunc->portlet_FileManager();
            return $portlet->getId();
        }
        return false;
    }
    
    public function getPortletConfiguration($workspaceId)
    {
        $portletConfiguration = array(
            '_blockTitleType' => 'custom',
            '_blockTitle' => '',
            'userFolder' => 0,
            'displayType' => 'icons',
            'hideDirectoryTree' => 0,
            'hideDirectoryToolbar' => 0,
            'firstColumnName' => 'name',
            'secondColumnName' => 'size',
            'thirdColumnName' => 'author',
            'fourthColumnName' => 'modifiedOn',
            'fifthColumnName' => 'configuration'
        );
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if($workspace->delegation == 0 || $workspace->name == ''){
                return $portletConfiguration;
            }

            $fileApi = filemanager_App();
            $folderSet = $fileApi->FolderSet();
            
            /* @var $workspaceBaseFolder filemanager_Folder */
            $workspaceBaseFolder = $folderSet->get($folderSet->id_dgowner->is($workspace->delegation));
            if($workspaceBaseFolder){
                $portletConfiguration['baseFolder'] = 'DG'.$workspace->delegation.'/'.$workspaceBaseFolder->folder;
            }
        }
        return $portletConfiguration;
    }

    /**
     * 
     * @param workspace_Workspace   $workspace
     * @param array                 $configuration
     * @throws Exception
     * @return int                  Id of the created folder
     */
    private function initializeFileManager(workspace_Workspace $workspace, $configuration = array())
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
        
        if (empty($workspace->name)) {
            throw new Exception('The folder name cannot be empty.');
        }
        
        //INITIALIZATION
        
        include_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';
        include_once $GLOBALS['babInstallPath'] . 'utilit/delegincl.php';
        
        bab_setCurrentUserDelegation($workspace->delegation);
        $oFileManagerEnv = getEnvObject();
        if (!$oFileManagerEnv->pathValid()) {
            throw new Exception('The folder path is not valid.');
        }
        
        $oFmFolderSet	= new BAB_FmFolderSet();
        $oName			= $oFmFolderSet->aField['sName'];
        $oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];
        
        $sName = replaceInvalidFolderNameChar($workspace->name);
        
        if (!isStringSupportedByFileSystem($sName)) {
            throw new Exception('The folder name "' . $sName .  '" contains characters not supported by the file system.');
        }
        
        $oCriteria = $oName->in($sName);
        $oCriteria = $oCriteria->_and($oIdDgOwner->in($workspace->delegation));
        $oFmFolder = $oFmFolderSet->get($oCriteria);
        if (!is_null($oFmFolder)) {
            throw new Exception('The folder already exists.');
        }
        
        $sFullPathName = BAB_FileManagerEnv::getCollectivePath($this->delegation) . $sName;
        
        if (!BAB_FmFolderHelper::createDirectory($sFullPathName)) {
            throw new Exception('Could not create folder');
        }
        
        //CREATE FOLDER
        
        //In database
        $version = isset($configuration['isVersionEnabled']) ? ($configuration['isVersionEnabled'] == 1 ? true : false) : false;
        $filenotify = isset($configuration['notifyMembers']) ? ($configuration['notifyMembers'] == 1 ? true : false) : false;
        
        $oFmFolder = new BAB_FmFolder();
        $oFmFolder->setApprobationSchemeId(0);
        $oFmFolder->setDelegationOwnerId($workspace->delegation);
        $oFmFolder->setName($sName);
        $oFmFolder->setRelativePath('');
        $oFmFolder->setFileNotify($filenotify);
        $oFmFolder->setActive(true);
        $oFmFolder->setAddTags(false);
        $oFmFolder->setVersioning($version);
        $oFmFolder->setHide(true);
        $oFmFolder->setAutoApprobation(false);
        
        $oFmFolder->save();
        
        //On server
        $sUploadPath = BAB_FmFolderHelper::getUploadPath();
        $sFullPathName = $sUploadPath . 'fileManager/collectives/DG' . $workspace->delegation . '/'.$sName;
        BAB_FmFolderHelper::createDirectory($sFullPathName);
        
        //RIGHTS
        
        $folderId = $oFmFolder->getId();
        
        $readersRightsTables = array(
            BAB_FMDOWNLOAD_GROUPS_TBL
        );
        $writersRightsTables = array(
            BAB_FMUPLOAD_GROUPS_TBL,
            BAB_FMDOWNLOAD_GROUPS_TBL,
            BAB_FMUPDATE_GROUPS_TBL
        );
        $administratorsRightsTables = array(
            BAB_FMUPLOAD_GROUPS_TBL,
            BAB_FMDOWNLOAD_GROUPS_TBL,
            BAB_FMUPDATE_GROUPS_TBL,
            BAB_FMMANAGERS_GROUPS_TBL
        );
        
        //  Groups
        //  The readers group is groupId
        $readersGroupId = $workspace->getDelegationGroupId();
        $writersGroupId = $workspace->getWritersGroupId();
        $administratorsGroupId = $workspace->getAdministratorsGroupId();
        
        foreach ($readersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $readersGroupId, $folderId);
        }
        foreach ($writersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $writersGroupId, $folderId);
        }
        foreach ($administratorsRightsTables as $rightsTable) {
            aclAdd($rightsTable, $administratorsGroupId, $folderId);
        }
        
        return $folderId;
    }
    
    public function setDefaultConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/FileManager/isWorkspaceEnabled', 1);
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/FileManager/isVersionEnabled', 1);
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/FileManager/maxCapacity', 0);
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/FileManager/notifyMembers', 0);
        }
    }
}