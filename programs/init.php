<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';


function filemanager_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();
    $functionalities->unregister('PortletBackend/FileManager');
    $functionalities->unregister('WorkspaceAddon/FileManager');

    bab_removeAddonEventListeners('prestameetin');

    return true;
}



function filemanager_upgrade($version_base, $version_ini)
{
    global $babDB;

    $babDB->db_query('SET FOREIGN_KEY_CHECKS = 0');


    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

    require_once dirname(__FILE__) . '/filemanager.php';

    $functionalities = new bab_functionalities();


    $addon = bab_getAddonInfosInstance('filemanager');
    $addonPhpPath = $addon->getPhpPath();

    if ($functionalities->registerClass('Func_App_Filemanager', $addonPhpPath . 'filemanager.php')) {
        echo(bab_toHtml('Functionality "Func_App_Filemanager" registered.'));
    }


    $App = filemanager_App();

    $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
    $customFieldSet = $App->CustomFieldSet();
    $sql = $mysqlbackend->setToSql($customFieldSet) . "\n";

    $fileSet = $App->FileSet();

    $synchronize = new bab_synchronizeSql();
    $synchronize->fromSqlString($sql);

    $synchronize = $App->synchronizeSql('filemanager_');
    $synchronize->addOrmSet($fileSet);


    @bab_functionality::includefile('PortletBackend');
    if (class_exists('Func_PortletBackend')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/portletbackend.class.php';
        $functionalities->registerClass('Func_PortletBackend_FileManager', $addonPhpPath . 'portletbackend.class.php');
    }

    @bab_functionality::includefile('WorkspaceAddon');
    if (class_exists('Func_WorkspaceAddon')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/workspaceaddon.class.php';
        $functionalities->registerClass('Func_WorkspaceAddon_FileManager', $addonPhpPath . 'workspaceaddon.class.php');
    }

    bab_removeAddonEventListeners('prestameetin');

    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'filemanager_onSiteMapItems', 'init.php');

    $addon->addEventListener('bab_eventRewrittenUrlRequested', 'filemanager_onRewrittenUrlRequested', 'init.php', 10);

    return true;
}



function filemanager_onRewrittenUrlRequested(bab_eventRewrittenUrlRequested $event)
{
    $elements = explode('/', $event->getRequestedUrl());

    if (($elements[0] === 'filemanager') && $elements[1] === 'wopi' && $elements[2] === 'files') {

        file_put_contents("php://stdout", __METHOD__ . ' : REQUEST_METHOD = ' . $_SERVER['REQUEST_METHOD'] . "\n");

        session_destroy();
        session_id($_GET['access_token']);
        session_start();

        $g = print_r($_GET, true);
        file_put_contents("php://stdout", "_GET: $g\n");

        $p = print_r($_POST, true);
        file_put_contents("php://stdout", "_POST: $p\n");

        $fileId = $elements[3];

        $App = filemanager_App();
        $fileSet = $App->FileSet();
        $file = $fileSet->get($fileId);

        if (!isset($file)) {
            header("HTTP/1.0 404 Not Found");
            die('404 Not Found');
        }

        require_once dirname(__FILE__) . '/wopi.class.php';
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                if (!isset($elements['4']) || $elements['4'] === '') {
                    filemanager_Wopi::checkFileInfo($file);
                } elseif ($elements['4'] == 'contents') {
                    filemanager_Wopi::getFile($file);
                }
                break;

            case 'POST':
                $tmpdir = $GLOBALS['babUploadPath'] . '/tmp/' . session_id();
                $tmpname = tempnam($tmpdir, 'tmp');
                file_put_contents("php://stdout", "tmpname: $tmpname\n");
                file_put_contents($tmpname, file_get_contents('php://input'));
                filemanager_Wopi::putFile($file, $tmpname);

                break;
        }



    }
    if (strpos($event->rw, '/wopi') !== false) {

    }
}


/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function filemanager_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includefile('Icons');

    $App = filemanager_App();


    $item = $event->createItem('filemanager_root');
    $item->setLabel($App->translate('Filemanager'));
    $item->setLink($App->Controller()->File()->displayFileManager()->url());
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->addIconClassname('apps-addon-filemanager');
    $event->addFolder($item);
}


