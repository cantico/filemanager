<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/file.ctrl.php';
require_once dirname(__FILE__) . '/file.php';
require_once dirname(__FILE__) . '/functions.php';


$W = bab_Widgets();


$W->Frame();



/**
 * Returns a frame width additional information about the file.
 *
 * @param bab_fileInfo $file
 * @return Widget_Frame|null
 */
function filemanager_filePropertiesFrame(filemanager_File $fmfile)
{
    $W = bab_Widgets();

    $propertiesBox = $W->Frame();
    $propertiesBox->addClass('filemanager-file-properties');
    $propertiesBox->setLayout(
        $W->HBoxItems()->setHorizontalSpacing(1, 'em')
    );

    $box = $W->VBoxItems()->setSizePolicy('widget-small');

    //File name
    $box->addItem(
        $W->LabelledWidget(
            filemanager_translate('Name'),
            $W->Label($fmfile->name)
        )
    );
    //File path
    if($fmfile->path != ''){
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Path'),
                $W->Label($fmfile->path)
            )
        );
    }
    //File size
    $box->addItem(
        $W->LabelledWidget(
            filemanager_translate('Size'),
            $W->Label($fmfile->getSize())
        )
    );
    //File author
    $fmAuthorName = bab_getUserName($fmfile->getAuthorId());
    if($fmAuthorName) {
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Uploaded by'),
                $W->Label($fmAuthorName)
            )
        );
    }
    //File modified by
    if($fmfile->getModifierId() != $fmfile->getAuthorId()){
        //Only display if the file has been modified by someone else than the author
        $fmEditorName = bab_getUserName($fmfile->getModifierId());
        if($fmEditorName){
            $box->addItem(
                $W->LabelledWidget(
                    filemanager_translate('Last modified by'),
                    $W->Label($fmAuthorName)
                )
            );
        }
    }
    //File creation date
    if(!empty($fmfile->getCreationDate())){
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Creation date'),
                $W->Label($fmfile->getCreationDate())
            )
        );
    }
    //File edition date
    if(!empty($fmfile->getModifiedDate())){
        //Only display if the date of creation and modification of the file are different
        if($fmfile->getModifiedDate() != $fmfile->getCreationDate()){
            $box->addItem(
                $W->LabelledWidget(
                    filemanager_translate('Modification date'),
                    $W->Label($fmfile->getModifiedDate())
                )
            );
        }
    }
    //File description
    if(!empty($fmfile->description)){
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Description'),
                $W->Label($fmfile->description)
            )
        );
    }
    //File tags
    $fileTags = $fmfile->getThesaurusTags();
    if(count($fileTags) > 0){
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Tags'),
                $tagBox = $W->FlowItems()
            )
        );
        foreach ($fileTags as $fileTag){
            $tagBox->addItem(
                $W->Label($fileTag)->addClass('tagBadge')
            );
        }
    }
    //File version
    if(!empty($fmfile->getVersion())){
        $box->addItem(
            $W->LabelledWidget(
                filemanager_translate('Version'),
                $W->Label($fmfile->getVersion())
            )
        );
        //If file version is different than 1.0, it means the file has at least one other version
        if($fmfile->getVersion() != '1.0'){
            $fileCtlr = filemanager_Controller()->File();
            $box->addItem(
                $W->Link(
                    filemanager_translate('Show file history'),
                    $fileCtlr->history($fmfile->id)
                )->setAjaxAction()
            );
        }
    }
    //File thumbnail
    $thumbnailImage = $W->ImageThumbnail(new bab_Path($fmfile->getFullPath(false)));
    $thumbnailImage->setThumbnailSize(200, 150);
    $propertiesBox->addItem($thumbnailImage);

    $propertiesBox->addItem($box);

    return $propertiesBox;
}



/**
 *
 * @param bab_FileInfo $file
 */
function filemanager_renameInstantLink($file, $userFolder)
{
    $W = bab_Widgets();

    if ($file->isDir()) {
        $linkLabel = filemanager_translate('Rename folder');
        $formTitle = filemanager_translate('Rename folder');
        $action = filemanager_Controller()->File()->renameFolder();
        $ajaxAction = filemanager_Controller()->File()->renameFolder();
    } else {
        $linkLabel = filemanager_translate('Rename file');
        $formTitle = filemanager_translate('Rename file');
        $action = filemanager_Controller()->File()->renameFile();
        $ajaxAction = filemanager_Controller()->File()->renameFile();
    }
    $renameLink = $W->Link(
        $linkLabel,
        ''
    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES);

    $renameForm = $W->Form();
    $renameForm->setLayout(
        $W->VboxItems(
            $W->LineEdit()
                ->setName('newName')
                ->setValue($file->getBasename())
                ->addClass('widget-fullwidth'),
            $W->SubmitButton()
                ->setLabel(filemanager_translate('Save'))
                ->setAction($action)
                ->setAjaxAction($ajaxAction, $renameLink)
        )->setVerticalSpacing(1, 'em')
    );
    $renameForm->setTitle($formTitle);
    $renameForm->setHiddenValue('tg', bab_rp('tg'));
    $renameForm->setHiddenValue('pathname', $file->getFmPathname());
    $renameForm->setHiddenValue('userFolder', $userFolder ? 1 : 0);

    $renameLink = $W->VboxItems(
        $renameLink
            ->addClass('widget-instant-button'),
        $renameForm
            ->addClass('widget-instant-form')
    )->addClass('widget-instant-container');

    return $renameLink;
}

function filemanager_changeDescriptionInstantLink(filemanager_File $file)
{
    $W = bab_Widgets();

    $linkLabel = filemanager_translate('Edit description');
    $formTitle = filemanager_translate('Edit description');

    $action = filemanager_Controller()->File()->changeDescription();
    $ajaxAction = filemanager_Controller()->File()->changeDescription();

    $changeDescriptionLink = $W->Link(
        $linkLabel,
        ''
    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES);

    $changeDescriptionForm = $W->Form();
    $changeDescriptionForm->setName('changeDesc');

    $changeDescriptionForm->setLayout(
        $W->VboxItems(
            $W->LineEdit()
                ->setName('description')
                ->setValue($file->description)
                ->addClass('widget-fullwidth'),
            $W->SubmitButton()
                ->setLabel(filemanager_translate('Save'))
                ->setAction($action)
                ->setAjaxAction($ajaxAction, $changeDescriptionLink)
        )->setVerticalSpacing(1, 'em')
    );
    $changeDescriptionForm->setTitle($formTitle);
    $changeDescriptionForm->setHiddenValue('tg', bab_rp('tg'));
    $changeDescriptionForm->setHiddenValue('file', $file->id);

    $changeDescriptionLink = $W->VboxItems(
        $changeDescriptionLink->addClass('widget-instant-button'),
        $changeDescriptionForm->addClass('widget-instant-form')
    )->addClass('widget-instant-container');

    return $changeDescriptionLink;
}

function filemanager_changeTagsInstantLink(filemanager_File $file)
{
    $W = bab_Widgets();

    $linkLabel = filemanager_translate('Edit tags');
    $formTitle = filemanager_translate('Edit tags');

    $action = filemanager_Controller()->File()->changeTags();
    $ajaxAction = filemanager_Controller()->File()->changeTags();

    $changeTagsLink = $W->Link(
        $linkLabel,
        ''
    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES);

    $allTags = filemanager_getThesaurusTags();

    $tagSelector = $W->Multiselect()->setName('tags');
    foreach ($allTags as $tag){
        $tagSelector->addOption($tag['id'], $tag['tag_name']);
    }

    $fileTags = $file->getThesaurusTags();
    $values = array();
    foreach ($fileTags as $tagId => $tagName){
        $values[] = $tagId;
    }
    $tagSelector->setValue($values);

    $changeTagsForm = $W->Form();
    $changeTagsForm->setName('changeTags');

    $changeTagsForm->setLayout(
        $W->VboxItems(
            $tagSelector->addClass('widget-fullwidth'),
            $W->SubmitButton()
                ->setLabel(filemanager_translate('Save'))
                ->setAction($action)
                ->setAjaxAction($ajaxAction, $changeTagsLink)
            )->setVerticalSpacing(1, 'em')
        );
    $changeTagsForm->setTitle($formTitle);
    $changeTagsForm->setHiddenValue('tg', bab_rp('tg'));
    $changeTagsForm->setHiddenValue('file', $file->id);

    $changeTagsLink = $W->VboxItems(
        $changeTagsLink->addClass('widget-instant-button'),
        $changeTagsForm->addClass('widget-instant-form')
    )->addClass('widget-instant-container');

    return $changeTagsLink;
}



/**
 * Returns the number of entries in the specified file.
 * The number is 0 for plain files.
 *
 * @param bab_FileInfo $file
 * @return int
 */
function filemanager_itemCount($file)
{
    if($file instanceof filemanager_UserFileInfo){
        $itemCount = 0;
        $oDir = dir($file->getPathname());
        while(false !== ($sEntry = $oDir->read()))
        {
            if($sEntry == '.' || $sEntry == '..' || $sEntry == BAB_FVERSION_FOLDER)
            {
                continue;
            }
            $itemCount++;

//@TODO Tenir compte du status du fichier.
        }
    }else{
        $pathName = $file->getFmPathname();
        $babDir = new bab_Directory();
        $iterator = $babDir->getEntries($pathName, 0);
        $itemCount = 0;
        if ($iterator instanceof bab_CollectiveDirIterator) {
            foreach ($iterator as $item) {

                if ($item->isDir()) {
                    $itemCount++;
                    continue;
                }

                $fmfile = $item->getFmFile();

                if ($fmfile && $fmfile->getState() !== 'D') {
                    $itemCount++;
                }
            }
        }
    }

    return $itemCount;
}

/**
 * Return the icon corresponding to the specified file.
 *
 * @param bab_FileInfo $file
 * @param int $thumbnailWidth
 * @param int $thumbnailHeight
 * @return Widget_Icon
 */
function filemanager_fileThumbnail(bab_FileInfo $file, $thumbnailWidth, $thumbnailHeight, $userFolder)
{
	$W = bab_Widgets();
	$T = filemanager_Thumbnailer();
	$F = bab_functionality::get('FileInfos');

	$fmDescription = '';
	$fmfile = $file->getFmFile(); /* $fmfile : object of BAB_FmFolderFile, object of BAB_FolderFile */
	if ($fmfile instanceof BAB_FolderFile) {
		$fmDescription = $fmfile->getDescription();
	}

	$filename = $file->getFilename();

	if ($file->isDir()) {

		$itemCount = filemanager_itemCount($file);

		if (($itemCount) === 0) {
			$subText = filemanager_translate('Empty');
		} else if (($itemCount) === 1) {
			$subText = sprintf(filemanager_translate('%d item'), $itemCount);
		} else {
			$subText = sprintf(filemanager_translate('%d items'), $itemCount);
		}

		$fileThumbnail = $W->Icon($filename . "\n" . $subText, 'widget-image');

		$fileThumbnail->setCanvasOptions(
		    Widget_Item::options()
		      ->width($thumbnailWidth, 'px')
		 );

		$fileThumbnail->addClass('filemanager-droppable');

		$fileThumbnail->setMetadata('filetype', 'folder');

		$fileThumbnail->setMetadata('movefileurl', filemanager_Controller()->File()->moveFile('__1__', '__2__', $userFolder)->url());
		$fileThumbnail->setMetadata('movefolderurl', filemanager_Controller()->File()->moveFolder('__1__', '__2__', $userFolder)->url());

	} else {

		$thumbnailUrl = null;
		$imageUrl = null;
		if ($F && $T) {
			// We have some problems with pdf, for now only make thumbnails for images.
			$mimetype = $F->getGenericClassName($file->getPathname());
			if ($mimetype == 'mimetypes-image-x-generic') {
				$T->setSourceFile($file->getPathname());
				$T->setResizeMode(Func_Thumbnailer::CROP_CENTER);
				$thumbnailUrl = $T->getThumbnail($thumbnailWidth, $thumbnailHeight);
				$T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);
				$imageUrl = $T->getThumbnail(2000, 2000);
			}
		}
		if ($imageUrl) {
			$fileThumbnail = $W->ImageZoomer($thumbnailUrl, $imageUrl, $filename);
			$fileThumbnail->setMetadata('filetype', 'file');
			$fileThumbnail->setCanvasOptions(Widget_Item::options()->width($thumbnailWidth, 'px')->height($thumbnailHeight, 'px'));
		} else {
			$fileThumbnail = null;
		}
	}

	if ($fileThumbnail) {
		$fileThumbnail->addAttribute('data-title', $fmDescription);
	}
	return $fileThumbnail;
}




/**
 *
 * @param bab_FileInfo $file
 * @param int $thumbnailWidth
 * @param int $thumbnailHeight
 * @return string|null|false
 */
function filemanager_getThumbnail(bab_FileInfo $file, $thumbnailWidth, $thumbnailHeight)
{
    $T = filemanager_Thumbnailer();
    $F = bab_functionality::get('FileInfos');
    $imageUrl = null;
    if ($F && $T) {
        // We have some problems with pdf, for now only make thumbnails for images.
        $mimetype = $F->getGenericClassName($file->getPathname());
        if ($mimetype == 'mimetypes-image-x-generic') {
            $T->setSourceFile($file->getPathname());
            $T->setBorder(1, '#aaaaaa', 1);
            $imageUrl = $T->getThumbnail($thumbnailWidth, $thumbnailHeight);
        }
    }

    return $imageUrl;
}



/**
 * Returns information about a file in an indexed array.
 *
 * @param bab_FileInfo $file
 * @return string[]
 */
function filemanager_getFileInformation(bab_FileInfo $file)
{
    $fileInformation = array();

    $fileInformation['filename'] = $file->getFilename();

    if ($file->isDir()) {
        $itemCount = filemanager_itemCount($file);

        if (($itemCount) === 0) {
            $sizeText = filemanager_translate('Empty');
        } else if (($itemCount) === 1) {
            $sizeText = sprintf(filemanager_translate('%d item'), $itemCount);
        } else {
            $sizeText = sprintf(filemanager_translate('%d items'), $itemCount);
        }
        $fileInformation['size'] = $sizeText;

        $fileInformation['mimetypeClassname'] = 'places-folder';
    } else {
        try {
            $fileSize = $file->getSize();
        } catch (Exception $e) {
            $fmPathName = $file->getFmPathname();
            $App = filemanager_App();
            $set = $App->FileSet();
            $set->fmPathname();
            $fmFile = $set->get($set->fmPathname->is($fmPathName));
            if($fmFile){
                $fileSize = $fmFile->size;
            }
            else{
                $fileSize = -1;
            }
        }
        if ($fileSize > 1024*1024) {
            $sizeText = round($fileSize / (1024*1024), 1) . ' ' . filemanager_translate('MB');
        } elseif ($fileSize > 1024) {
            $sizeText = round($fileSize / 1024) . ' ' . filemanager_translate('KB');
        } else {
            $sizeText = $fileSize . ' ' . filemanager_translate('Bytes');
        }

        $fileInformation['mimetypeClassname'] = 'mimetypes-unknown';
        $F = bab_functionality::get('FileInfos');
        if ($F) {
            $fileInformation['mimetypeClassname'] = $F->getGenericClassName($file->getPathname());
        }

        $fileInformation['size'] = $sizeText;
    }

    return $fileInformation;
}



/**
 * Return the icon corresponding to the specified file.
 *
 * @param bab_FileInfo $file
 * @param string $displayType   filemanager_CtrlFile::DISPLAY_xxx
 * @param int $thumbnailWidth
 * @param int $thumbnailHeight
 * @return Widget_Item
 */
function filemanager_fileIcon(bab_FileInfo $file, $displayType, $thumbnailWidth, $thumbnailHeight, $userFolder)
{
    $W = bab_Widgets();

    $fileInformation = filemanager_getFileInformation($file);
    $imageUrl = filemanager_getThumbnail($file, $thumbnailWidth, $thumbnailHeight);

    if ($imageUrl) {
        $image = $W->Image($imageUrl, $fileInformation['filename']);
    } else {
        $image = $W->Frame('')->addClass($fileInformation['mimetypeClassname']);
    }
    $image->addClass('icon');

    if ($displayType === filemanager_CtrlFile::DISPLAY_COMPACT || $displayType === filemanager_CtrlFile::DISPLAY_DETAILS) {
        $fileIcon = $W->FlowItems(
            $image,
            $W->Label($fileInformation['filename'])
        )->setVerticalAlign('middle')
        ->addClass('widget-nowrap');
    } else {
        $fileIcon = $W->VBoxItems(
            $image,
            $W->Label($fileInformation['filename']),
            $W->Label($fileInformation['size'])->addClass('widget-icon-sub-label')
        )->addClass('widget-align-center');
    }

    if ($file->isDir()) {
        $fileIcon->addClass('filemanager-droppable');
        $fileIcon->setMetadata('filetype', 'folder');
        $fileIcon->setMetadata('movefileurl', filemanager_Controller()->File()->moveFile('__1__', '__2__', $userFolder)->url());
        $fileIcon->setMetadata('movefolderurl', filemanager_Controller()->File()->moveFolder('__1__', '__2__', $userFolder)->url());
    } else {
        $fileIcon->setMetadata('filetype', 'file');
    }

    $fileIcon->addClass('filemanager-file-icon');

    return $fileIcon;
}



/**
 *
 * @param bab_FileInfo $file
 *
 * @return Widget_Menu
 */
function filemanager_addContextMenu(bab_FileInfo $file, $fileIcon)
{
    $W = bab_Widgets();

    $ctrlFile = filemanager_Controller()->File();

    $userFolder = false;
    if ($file instanceof filemanager_UserFileInfo) {
        $userFolder = true;
    }

    $pathname = $file->getFmPathname();
    $path = dirname($pathname);

    $contextMenu = $W->Menu();
    $contextMenu->setLayout($W->FlowLayout())
        ->addClass(Func_Icons::ICON_LEFT_16);

    if (filemanager_canRenameFileInFolder($path, $userFolder)) {
        //Rename link
        $contextMenu->addItem(filemanager_renameInstantLink($file, $userFolder));
    }

    if ($file->isDir()) {
        //Delete folder link
        if (filemanager_canDeleteFolderInFolder($path, $userFolder)) {
            $deleteLink = $W->Link(filemanager_translate('Delete folder'), $ctrlFile->deleteFolder($pathname, $userFolder))
                ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                ->setAjaxAction();
            $deleteLink->setConfirmationMessage(filemanager_translate('Are you sure you want to delete this folder?'));
            $contextMenu->addItem($deleteLink);
        }
        //Download folder zip link
        $fileInfos = $file->getFileInfo();
        $downloadZip = $W->Link(filemanager_translate('Download folder archive'), $ctrlFile->downloadZipFolder($fileInfos->getPathname()))
            ->addClass('icon', Func_Icons::ACTIONS_ARCHIVE_CREATE)
            ->setOpenMode(Widget_Link::OPEN_POPUP);
        $contextMenu->addItem($downloadZip);
    } else {
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPathname();
        $set->fmPath();

        /* @var $fmfile filemanager_File */
        $fmfile = $set->get($set->fmPathname->is($file->getFmPathname()));
        if ($fmfile) {
            if(filemanager_canUpdateFileInFolder($path, $userFolder)){
                //Description link
                $contextMenu->addItem(filemanager_changeDescriptionInstantLink($fmfile));
                //Tags Link
                $contextMenu->addItem(filemanager_changeTagsInstantLink($fmfile));

                $wopisrc = $GLOBALS['babUrl'] . 'filemanager/wopi/files/' . $fmfile->id;
                $contextMenu->addItem(
                    $W->Link(
                        filemanager_translate('Edit...'),
                        //'//localhost:9980/loleaflet/dist/loleaflet.html?file_path=file://' . $fullpath . '&host=wss://localhost:9980'
                        'http://localhost:9980/loleaflet/dist/loleaflet.html?WOPISrc=' . $wopisrc . '&access_token=' . session_id()
                    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
            }
            if($fmfile->isTrashed()){
                //Permananent delete link
                $deleteLink = $W->Link(filemanager_translate('Delete file permanently'), $ctrlFile->deleteFile($pathname, true, $userFolder))
                    ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction();
                $deleteLink->setConfirmationMessage(filemanager_translate('Are you sure you want to permanently delete this file and all its versions?'));
                $contextMenu->addItem($deleteLink);
                //Restore link
                $restoreLink = $W->Link(filemanager_translate('Restore this file'), $ctrlFile->restoreFile($fmfile->id))
                    ->addClass('icon', Func_Icons::ACTIONS_VIEW_REFRESH)
                    ->setAjaxAction();
                $contextMenu->addItem($restoreLink);
            }
            else{
                //Extract zip link
                if ($file->getExtension() === 'zip') {
                    if (filemanager_canCreateFolderInFolder($path, $userFolder)) {
                        $unzipFileLink = $W->Link(filemanager_translate('Extract file here'), $ctrlFile->unzipFile($pathname))
                        ->addClass('icon', Func_Icons::ACTIONS_ARCHIVE_EXTRACT)
                        ->setAjaxAction();
                        $unzipFileLink->setConfirmationMessage(filemanager_translate('File will be extracted here'));
                        $contextMenu->addItem($unzipFileLink);
                    }
                }

                //Download file link
                if (filemanager_canDownloadFileFromFolder(str_replace('TR', '', $fmfile->fmPath), $userFolder)) {
                    $downloadLink = $W->Link(filemanager_translate('Download file'), $ctrlFile->displayFile($pathname, false, $userFolder))
                    ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_SAVE);
                    $contextMenu->addItem($downloadLink);
                }

                //Trash file link
                if (filemanager_canDeleteFileInFolder($path, $userFolder)) {
                    $deleteLink = $W->Link(filemanager_translate('Place in trash'), $ctrlFile->deleteFile($pathname, false, $userFolder))
                    ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction();
                    $deleteLink->setConfirmationMessage(filemanager_translate('Are you sure you want to trash this file?'));
                    $contextMenu->addItem($deleteLink);
                }
                //Versioning
                $fmfolder = $fmfile->getFolderInfos();
                if($fmfolder->isVersioned() && filemanager_canUpdateFileInFolder($path, $userFolder)){
                    //Lock file link
                    if(!$fmfile->isLocked()){
                        $lockLink = $W->Link(
                            filemanager_translate('Lock this file'),
                            $ctrlFile->lockFile($fmfile->id)
                        )->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
                        ->setAjaxAction();
                        $contextMenu->addItem($lockLink);
                    }
                    //Unlock file link
                    else if($fmfile->canBeUnlockedByUser()){
                        $unlockLink = $W->Link(
                            filemanager_translate('Unlock this file'),
                            $ctrlFile->unlockFile($fmfile->id)
                        )->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
                        ->setAjaxAction();
                        $contextMenu->addItem($unlockLink);
                    }
                    //Locked status
                    else{
                        $userName = bab_getUserName($fmfile->lockedBy());
                        $msg = $userName ? sprintf(filemanager_translate('This file has been locked by %s'), $userName) : filemanager_translate('This file is locked');
                        $contextMenu->addItem(
                            $W->Label($msg)->addClass('icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
                        );
                    }
                }
            }

            // Additional information of the file in file manager

            $htmlTooltip = filemanager_filePropertiesFrame($fmfile);
            $contextMenu->addItem($htmlTooltip);
        }
    }

    return $contextMenu;
}



/**
 *
 * @param string $portletId
 * @param string $path
 * @param string $thumbnailSize
 * @param bool $userFolder
 */
function filemanager_folderThumbnailView($portletId, $path, $thumbnailSize = null, $userFolder = false)
{
    $W = bab_Widgets();
    $ctrlFile = filemanager_Controller()->File();

    $thumbnailWidth = $thumbnailSize;
    $thumbnailHeight = $thumbnailSize;
    $layout = $W->FlowLayout(); //->setSpacing(4, 'px');

    $listView = $W->Frame();
    $listView->setLayout($layout);
    $listView->addClass('widget-list-view', 'filemanager-droppable');

    $listView->setMetadata('pathname', urlencode($path));

    $listView->setMetadata('movefileurl', $ctrlFile->moveFile('__1__', '__2__', $userFolder)->url());
    $listView->setMetadata('movefolderurl', $ctrlFile->moveFolder('__1__', '__2__', $userFolder)->url());

    $files = filemanager_listFolder($path, 0, $userFolder); /* search files with bab_CollectiveDirIterator() */

    $nbItems = 0;
    foreach ($files as $file) {

        /* @var $file bab_FileInfo */

       	$fileIcon = filemanager_fileThumbnail($file, $thumbnailWidth, $thumbnailHeight, $userFolder);
       	if (!isset($fileIcon)) {
            continue;
        }
        $fileIcon->setMetadata('pathname', urlencode($file->getFmPathname()));

        $fileIcon->addClass('filemanager-draggable');

        if ($file->isDir()) {

            $link = $W->Link(
                $fileIcon,
                $ctrlFile->browse($file->getFmPathname())
            );
            $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $file->getFmPathname()), $fileIcon);

        } else {

            $link = $W->Link(
                $fileIcon,
                $ctrlFile->displayFile($file->getFmPathname(), true, $userFolder)
            );
        }

        $item = $W->FlowItems();
        $item->addClass('widget-actions-target');

        $contextMenu = filemanager_addContextMenu($file, $fileIcon);
        $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
        if (!$menuEmpty) {
            $item->addItem(
                $contextMenu->setSizePolicy('widget-actions')
            );
        }

        $item->addItem($link);

        $listView->addItem($item);

//         $contextMenu = filemanager_addContextMenu($file, $fileIcon);
//         $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
//         if (!$menuEmpty) {
//             $contextMenu->attachTo($fileIcon);
//             $listView->addItem($contextMenu);
//         }
//         $listView->addItem($item);
        $nbItems++;
    }

    if ($nbItems === 0) {
        $listView->addItem(
            $W->Label(filemanager_translate('The folder is empty'))
        )->addClass('filemanager-empty-folder');
    }

    return $listView;
}



/**
 * Returns a listview containing icons representing the content of the specified folder.
 *
 * @param string    $portletId
 * @param string    $path		  The path of the folder that should be displayed in the listview.
 * @param int       $iconSize
 * @param string    $displayType
 * @param bool      $userFolder
 *
 * @return Widget_Frame
 */
function filemanager_folderIconView($portletId, $path, $iconSize = 32, $displayType = 'icons', $userFolder = false)
{
    $W = bab_Widgets();
    $ctrlFile = filemanager_Controller()->File();

    switch ($displayType) {
        case filemanager_CtrlFile::DISPLAY_COMPACT:
            if ($iconSize == 16) {
                $iconClass = Func_Icons::ICON_LEFT_16;
                $thumbnailWidth = 14;
                $thumbnailHeight = 14;
            } elseif ($iconSize == 24) {
                $iconClass = Func_Icons::ICON_LEFT_24;
                $thumbnailWidth = 22;
                $thumbnailHeight = 22;
            } elseif ($iconSize == 32) {
                $iconClass = Func_Icons::ICON_LEFT_32;
                $thumbnailWidth = 30;
                $thumbnailHeight = 30;
            } elseif ($iconSize == 48) {
                $iconClass = Func_Icons::ICON_LEFT_48;
                $thumbnailWidth = 46;
                $thumbnailHeight = 46;
            }
            $layout = $W->FlowLayout(); //->setSpacing(4, 'px');
            $layout->setVerticalAlign('middle');
            break;
        case filemanager_CtrlFile::DISPLAY_ICONS:
        default:
            if ($iconSize == 16) {
                $iconClass = Func_Icons::ICON_TOP_16;
                $thumbnailWidth = 92;
                $thumbnailHeight = 6;
            } elseif ($iconSize == 24) {
                $iconClass = Func_Icons::ICON_TOP_24;
                $thumbnailWidth = 92;
                $thumbnailHeight = 14;
            } elseif ($iconSize == 32) {
                $iconClass = Func_Icons::ICON_TOP_32;
                $thumbnailWidth = 92;
                $thumbnailHeight = 22;
            } elseif ($iconSize == 48) {
                $iconClass = Func_Icons::ICON_TOP_48;
                $thumbnailWidth = 92;
                $thumbnailHeight = 38;
            }
            $layout = $W->FlowLayout(); //->setSpacing(4, 'px');
            $layout->setVerticalAlign('top');
            break;
    }



    $listView = $W->Frame()
        ->setLayout($layout)
		->addClass('widget-list-view', 'filemanager-droppable')
        ->addClass($iconClass);

    $listView->setMetadata('pathname', urlencode($path));

    $listView->setMetadata('movefileurl', $ctrlFile->moveFile('__1__', '__2__', $userFolder)->url());
    $listView->setMetadata('movefolderurl', $ctrlFile->moveFolder('__1__', '__2__', $userFolder)->url());

    if(filemanager_File::isTrashPath($path)){
        $files = filemanager_listTrash($path);
    }
    else{
        $files = filemanager_listFolder($path, 0, $userFolder); /* search files with bab_CollectiveDirIterator() */
    }

    $nbItems = 0;
    foreach ($files as $file) {

        /* @var $file bab_FileInfo */

        $fileIcon = filemanager_fileIcon($file, $displayType, $thumbnailWidth, $thumbnailHeight, $userFolder);

        $fileIcon->setMetadata('pathname', urlencode($file->getFmPathname()));

        $fileIcon->addClass('filemanager-draggable');
        $fileIcon->addClass('filemanager-icon');

        if ($file->isDir()) {
            $link = $W->Link(
                $fileIcon,
                $ctrlFile->browse($file->getFmPathname())
            );
            $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $file->getFmPathname()), $fileIcon);
        } else {
            $link = $W->Link(
                $fileIcon,
                $ctrlFile->displayFile($file->getFmPathname(), true, $userFolder)
            );
        }

        $item = $W->FlowItems();
        $item->addClass('widget-actions-target');

        $contextMenu = filemanager_addContextMenu($file, $fileIcon);
        $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
        if (!$menuEmpty) {
            $item->addItem(
                $contextMenu->setSizePolicy('widget-actions')
            );
        }

        $item->addItem($link);

        $listView->addItem($item);
        $nbItems++;
    }

    if ($nbItems === 0) {
        $listView->addItem(
            $W->Label(filemanager_translate('The folder is empty'))
        )->addClass('filemanager-empty-folder');
    }

    return $listView;
}






/**
 * Returns a table view with each row representing the content of the specified folder.
 *
 * @param string $portletId
 * @param string $path		  The path of the folder that should be displayed in the tableview.
 * @param int    $iconSize
 *
 * @return Widget_Frame
 */
function filemanager_folderTableView($portletId, $path, $iconSize = null, $userFolder, $columnNames = null)
{
    $W = bab_Widgets();
    $ctrlFile = filemanager_Controller()->File();

    switch ($iconSize) {
        case 48:
            $iconClass = Func_Icons::ICON_LEFT_48;
            $thumbnailWidth = 42;
            $thumbnailHeight = 42;
            break;

        case 32:
            $iconClass = Func_Icons::ICON_LEFT_32;
            $thumbnailWidth = 28;
            $thumbnailHeight = 28;
            break;

        case 24:
            $iconClass = Func_Icons::ICON_LEFT_24;
            $thumbnailWidth = 20;
            $thumbnailHeight = 20;
            break;

        default:
        case 16:
            $iconClass = Func_Icons::ICON_LEFT_16;
            $thumbnailWidth = 14;
            $thumbnailHeight = 14;
        break;
    }


    $listView = $W->TableView(/*__METHOD__ . $portletId*/);
    $listView->addClass($iconClass);


    $listView->addSection('header', null, 'widget-table-header');

    $listView->addSection('body', null, 'widget-table-body');


    // HEADER
    $listView->setCurrentSection('header');

    //$columnList = ['Title of the column' => 'Class to add to the column']
    $columnList = array();
    if(isset($columnNames) && !empty($columnNames)){
        foreach ($columnNames as $columnName){
            switch($columnName){
                case 'name':
                    $columnList['Name'] = '';
                    break;
                case 'size':
                    $columnList['Size'] = 'widget-align-right';
                    break;
                case 'author':
                    $columnList['Author'] = '';
                    break;
                case 'modifiedOn':
                    $columnList['Modified on'] = '';
                    break;
                case 'configuration':
                    $columnList['Configuration'] = '';
                    break;
            }
        }
    }
    else{
        $columnList = array(
            'Name' => '',
            'Size' => 'widget-align-right',
            'Author' => '',
            'Modified on' => '',
            'Configuration' => ''
        );
    }

    $columnIndex = 1;
    foreach ($columnList as $name => $class){ //Add the columns
        if($name === 'Configuration'){
            $listView->addItem($W->Label(''), 0, $columnIndex);
            $listView->addColumnClass($columnIndex, 'widget-1em');
        }
        else{
            $listView->addItem($W->Label(filemanager_translate($name)), 0, $columnIndex);
            $listView->addColumnClass($columnIndex, $class);
        }
        $columnIndex ++;
    }

    //Add the context menu column



    // BODY
    $listView->setCurrentSection('body');

    $listView->setMetadata('movefileurl', filemanager_Controller()->File()->moveFile('__1__', '__2__', $userFolder)->url());
    $listView->setMetadata('movefolderurl', filemanager_Controller()->File()->moveFolder('__1__', '__2__', $userFolder)->url());

    if(!filemanager_File::isTrashPath($path)){
        $files = filemanager_listFolder($path, 0, $userFolder); /* search files with bab_CollectiveDirIterator() */
    }
    else{
        $files = filemanager_listTrash($path);
    }


    $row = 0;
    $App = filemanager_App();
    $set = $App->FileSet();
    $set->fmPathname();

    foreach ($files as $file) {
        /* @var $file bab_FileInfo */

        $fileIcon = filemanager_fileIcon($file, filemanager_CtrlFile::DISPLAY_DETAILS, $thumbnailWidth, $thumbnailHeight, $userFolder);

        $fileIcon->setMetadata('pathname', urlencode($file->getFmPathname()));

        $fileIcon->addClass('filemanager-draggable');

        $fileInformation = filemanager_getFileInformation($file);

        $theFileIsDeleted = false;
        $fmAuthorName = '';
        $modificationDate = '';


        if ($file->isDir()) {

            $link = $W->Link(
                $fileIcon,
                $ctrlFile->browse($file->getFmPathname())
            );
            $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $file->getFmPathname()), $fileIcon);

        } else {

            $fmfile = $file->getFmFile(); /* $fmfile : object of BAB_FmFolderFile, object of BAB_FolderFile */
            if (!$fmfile) {
                $fmPathName = $file->getFmPathname();
                if(filemanager_File::isTrashPath($fmPathName)){
                    $fmPathName = substr($fmPathName, 2);
                }
                $fmfile = $set->get($set->fmPathname->is($fmPathName));
                if(!$fmfile){
                    continue;
                }
                $fileInformation['size'] = $fmfile->getSize();
            }

            $fmState = $fmfile->getState(); /* '', D(deleted) or X(cutted) */
            if ($fmState === 'D' && !filemanager_File::isTrashPath($path)) {
                continue;
            }

            $fmAuthorId = $fmfile->getAuthorId();
            $fmAuthorName = bab_getUserName($fmAuthorId, true);
            $fmModificationDate = $fmfile->getModifiedDate(); /* Format : 2010-09-21 16:32:13 */
            $modificationDate = bab_shortDate(bab_mktime($fmModificationDate), true);

            $link = $W->Link(
                $fileIcon,
                $ctrlFile->displayFile($file->getFmPathname(), true, $userFolder)
            );

        }

        $link->addClass('filemanager-file');

        $contextMenu = filemanager_addContextMenu($file, $fileIcon);

        // Deleted files don't appear.
        if (!$theFileIsDeleted) {

            $columnIndex = 1;
            foreach ($columnList as $name => $class) {
                $item = null;
                switch ($name) {
                    case 'Name':
                        $item = $link;
                        break;
                    case 'Size':
                        $item = $W->Label($fileInformation['size']);
                        break;
                    case 'Author':
                        $item = $W->Label($fmAuthorName);
                        break;
                    case 'Modified on':
                        $item = $W->Label($modificationDate);
                        break;
                    case 'Configuration':
                        $item = $contextMenu;
                        break;
                }
                $listView->addItem($item, $row, $columnIndex);
                $columnIndex++;
            }
            $row++;
        }
    }

    return $listView;
}





/**
 * @param string $path
 * @return Widget_SimpleTreeview
 */
function filemanager_folderTreeView($portletId, $path, $userFolder)
{
    require_once dirname(__FILE__) . '/file.php';
    require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';

    $App = filemanager_App();

    $ctrlFile = $App->Controller()->File();

    $W = bab_Widgets();

    $treeview = $W->SimpleTreeview('filemanager_treeview' . $portletId)
	    ->hideToolbar()
        ->addClass(Func_Icons::ICON_LEFT_16, 'filemanager-side-panel');

    $treeview->setPersistent();

    $rootPath = $currentPath = filemanager_getConf($portletId . '_baseFolder');
    if($userFolder){
        $currentPath = '';
        $rootPath = 'U'.bab_getUserId();
    }
    $files = filemanager_listFolderRecursive(dirname($currentPath), $userFolder);

    foreach ($files as $file) {
        if (!$file->isDir()) {
            continue;
        }
        $fmPathname = $file->getFmPathname();

        if ($fmPathname == $rootPath) {
            if($userFolder){
                $folderTitle = bab_toHtml(filemanager_translate('Personal folder'));
            }else{
                $folderTitle = bab_toHtml($file->getFilename());
            }
            $parentId = null;
        } else {
            $folderTitle = bab_toHtml($file->getFilename());
            $parentId = urlencode(dirname($fmPathname));
        }

        $folder = $treeview->createElement(
            urlencode($fmPathname),
            'directory',
            $folderTitle,
            $folderTitle,
            $ctrlFile->browse($fmPathname)->url()
        );
        $link = $W->Link(
            $folderTitle,
            $ctrlFile->browse($fmPathname)
        );
        $link->addClass('icon ' . Func_Icons::PLACES_FOLDER);
        $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $fmPathname));

        $link->addClass('filemanager-droppable');
        $link->setMetadata('filetype', 'folder');
        $link->setMetadata('pathname', urlencode($file->getFmPathname()));
        $link->setMetadata('movefileurl', $ctrlFile->moveFile('__1__', '__2__', $userFolder)->url());
        $link->setMetadata('movefolderurl', $ctrlFile->moveFolder('__1__', '__2__', $userFolder)->url());

        $folder->setItem($link);

        $treeview->appendElement($folder, $parentId);
    }

//    if (workspace_canViewTrash()) {
//         $workspaceId = workspace_getCurrentWorkspace();
//         $trashPath = workspace_getWorkspaceTrashPath($workspaceId);


    if (isset($file)) {
        if(filemanager_canUpdateFolder($path, $userFolder)){
            $fmPathname = $file->getFmPathname();
            $trashFmPathname = filemanager_File::getRootPath('TR'.$fmPathname);
            $trash = $treeview->createElement(
                urlencode(''),
                'directory',
                bab_toHtml($App->translate('Trash')),
                bab_toHtml($App->translate('Trash')),
                $ctrlFile->browse($trashFmPathname)->url()
            );

            $link = $W->Link(
                $App->translate('Trash'),
                $ctrlFile->browse($trashFmPathname)
            );
            $link->addClass('icon ' . Func_Icons::PLACES_USER_TRASH);
            $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $trashFmPathname));

            $link->addClass('filemanager-droppable');
            $link->setMetadata('filetype', 'folder');
            $link->setMetadata('pathname', urlencode($trashFmPathname));
            $link->setMetadata('movefileurl', $ctrlFile->moveFile('__1__', '__2__', $userFolder)->url());
            $link->setMetadata('movefolderurl', $ctrlFile->moveFolder('__1__', '__2__', $userFolder)->url());

            // $icon = $W->Icon($App->translate('Trash'), Func_Icons::PLACES_USER_TRASH);

            //         if ($trashPath !== $path) {
            //             $icon->addClass('workspace-droppable');
            //             $icon->setMetadata('filetype', 'folder');
            //             $icon->setMetadata('pathname', $trashPath);
            //         }
            $trash->setItem($link);

            $treeview->appendElement($trash, null);
        }
    }

    $treeview->highlightElement(urlencode($path));

    return $treeview;
}







/**
 * Creates a location bar corresponding to the specified path.
 *
 * @param string $path	The path is something like "DG12/Workspace name/subfolder1/subfolder2"
 *
 * @return Widget_FlowLayout
 */
function filemanager_folderLocationBar($portletId, $path, $userFolder)
{
    require_once dirname(__FILE__) . '/file.ctrl.php';
    require_once dirname(__FILE__) . '/file.php';
    require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';

    $W = bab_Widgets();

    $rootPath = dirname(filemanager_getConf($portletId . '_baseFolder'));

    $bar = $W->FlowLayout();
    if(filemanager_File::isTrashPath($path)){
        $rootPath = rtrim(filemanager_File::getRootPath($path), '/').'/'; //Ensure we have a trailing slash
        $link = $W->Link(
            filemanager_translate('Trash'),
            filemanager_Controller()->File()->browse($rootPath)
        );

        $link->setSizePolicy('filemanager-locationbar-item');
        $link->setAjaxAction(filemanager_Controller()->File()->changeCurrentFolder($portletId, $rootPath), $link);
        $link->addClass('filemanager-droppable', 'btn');
        $link->setMetadata('filetype', 'folder');
        $link->setMetadata('pathname', urlencode($rootPath));
        $link->setMetadata('movefileurl', filemanager_Controller()->File()->moveFile('__1__', '__2__', $userFolder)->url());
        $link->setMetadata('movefolderurl', filemanager_Controller()->File()->moveFolder('__1__', '__2__', $userFolder)->url());
        $bar->addItem($link, 0);
    }
    else{
        for (; $path != $rootPath && $path != '.'; $path = dirname($path)) {
            $folderName = basename($path);

            $link = $W->Link(
                $folderName,
                filemanager_Controller()->File()->browse($path)
            );

            $link->setSizePolicy('filemanager-locationbar-item');

            $link->setAjaxAction(filemanager_Controller()->File()->changeCurrentFolder($portletId, $path), $link);
            $link->addClass('filemanager-droppable', 'btn');
            $link->setMetadata('filetype', 'folder');
            $link->setMetadata('pathname', urlencode($path));
            $link->setMetadata('movefileurl', filemanager_Controller()->File()->moveFile('__1__', '__2__', $userFolder)->url());
            $link->setMetadata('movefolderurl', filemanager_Controller()->File()->moveFolder('__1__', '__2__', $userFolder)->url());
            $bar->addItem($link, 0);
        }
    }

    return $bar;
}







/**
 *
 * @param string $portletId
 * @param string $path
 * @param string $displayType
 * @param string $keywords
 * @param string $hideDirectoryTree
 * @param string $hideDirectoryToolbar
 * @param string $userFolder
 *
 * @return Widget_VBoxLayout
 */
function filemanager_folderSplitView($portletId, $path, $displayType = null, $keywords = null, $hideDirectoryTree = true, $hideDirectoryToolbar = false, $userFolder = false, $columnNames = null)
{
    require_once dirname(__FILE__) . '/file.ctrl.php';
    require_once dirname(__FILE__) . '/file.php';

    $ctrlFile = filemanager_Controller()->File();

    $canCreateFolder = filemanager_canCreateFolderInFolder($path, $userFolder);
    $canUploadFile = filemanager_canUploadFileInFolder($path, $userFolder);

    $W = bab_Widgets();
    // Toolbar

    $iconSize = filemanager_getConf($portletId . '_displayIconSize', 'medium');

    $toolbar = filemanager_Toolbar();

    $addMenu = $W->Menu(
        null,
        $W->VBoxLayout()
    );
    $addMenu->setButtonClass('widget-actionbutton btn icon ' . Func_Icons::ACTIONS_LIST_ADD);
    $addMenu->setButtonLabel('');


    $newFolderForm = $W->Form();
    $newFolderForm->setName('folder');
    $newFolderForm->setLayout(
        $W->VBoxItems(
            $W->LineEdit()->setName('name')
                ->addClass('widget-fullwidth'),
            $W->SubmitButton()
                ->setLabel(filemanager_translate('Save'))
                ->setAction($ctrlFile->createFolder())
                ->setAjaxAction($ctrlFile->createFolder(), $toolbar)
        )->setVerticalSpacing(1, 'em')
    );
    $newFolderForm->setTitle(filemanager_translate('Create folder'));
    $newFolderForm->setHiddenValue('tg', bab_rp('tg'));
    $newFolderForm->setHiddenValue('folder[path]', $path);
    $newFolderForm->setHiddenValue('folder[user]', $userFolder);


    $newFolderButton = $W->VBoxItems(
        $W->Link(filemanager_translate('Create folder'), '')
            ->addClass('icon', Func_Icons::ACTIONS_FOLDER_NEW, 'widget-instant-button'),
        $newFolderForm
            ->addClass('widget-instant-form')
    )->addClass('widget-instant-container');

    $addon = bab_getAddonInfosInstance('filemanager');
    $tempPath = new bab_Path($addon->getUploadPath(), 'temp', $portletId);


    $uploadFileButton = $W->FilePicker();
    $uploadFileButton
        ->oneFileMode(false)
        ->hideFiles()
        ->addClass('icon', 'widget-link', Func_Icons::ACTIONS_LIST_ADD)
        ->setName('file')
        ->setFolder($tempPath);

    $uploadStatusBox = filemanager_Controller()->File(false)->uploadStatus();

    $uploadFileButton->setAjaxAction($ctrlFile->addFile($portletId, $path, $userFolder), array($uploadFileButton, $uploadStatusBox));




    $configurationMenu = filemanager_configurationMenu($portletId);



    $addMenu->addClass(Func_Icons::ICON_LEFT_16);

    if ($canCreateFolder) {
        $addMenu->addItem($newFolderButton);
    }

    if ($canUploadFile) {
        $addMenu->addItem($uploadFileButton);
    }


    $searchForm = null;
    if (!$userFolder) {
        $searchForm = $W->Form();
        $searchForm->setName('search');
        $searchForm->setReadOnly();
        $searchForm->setLayout($W->FlowLayout());
        $searchForm->addItem(
            $searchEdit = $W->LineEdit()
                ->setSize(20)
                ->setName('keywords')
                ->addClass('form-control', 'icon', Func_Icons::ACTIONS_EDIT_FIND)
                ->setPlaceHolder(filemanager_translate('Search...'))
        );
        if (isset($keywords)) {
            $searchEdit->setValue($keywords);
        }

        $searchForm->setAjaxAction($ctrlFile->fileSearch($portletId), null, 'change');
    }

    $rightBox = $W->FlowItems(
        $searchForm,
        $W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp()),
        $configurationMenu
    );


    $toolbar->addItem(
        $rightBox
            ->setSizePolicy('pull-right')
    );



    $locationbar = filemanager_folderLocationBar($portletId, $path, $userFolder);

    $treeview = null;

    if (!$hideDirectoryTree) {
        $treeview = filemanager_folderTreeView($portletId, $path, $userFolder);
        $treeview->setSizePolicy(Widget_SizePolicy::MINIMUM . ' ' . 'filemanager-side-panel-container');
        $treeview->addClass('filemanager-resizable-panel');
    }

    $toolbar->addItem($locationbar);

    if(!filemanager_File::isTrashPath($path)){
        $toolbar->addItem($addMenu);
    }

    if ($hideDirectoryToolbar) {
        $toolbar = null;
    }


    $pixelSizes = array(
        'tiny' => 16,
        'small' => 24,
        'medium' => 32,
        'large' => 48,
    );

    $pixelSize = 32;
    if (isset($pixelSizes[$iconSize])) {
        $pixelSize = $pixelSizes[$iconSize];
    }

    if (isset($keywords)) {
        $listview = filemanager_filesSearchResultList($portletId, $path, $displayType, $keywords, $userFolder);
    } else {
        if ($displayType === 'details') {
            $listview = filemanager_folderTableView($portletId, $path, $pixelSize, $userFolder, $columnNames);
        } elseif ($displayType === 'thumbnail') {
            $listview = filemanager_folderThumbnailView($portletId, $path, $pixelSize * 4, $userFolder);
        } else {
            $listview = filemanager_folderIconView($portletId, $path, $pixelSize, $displayType, $userFolder);
        }
    }

    $uploadFileButton->setAssociatedDropTarget($listview);

    $explorer = $W->VBoxItems(
        $toolbar,
        $W->HBoxItems(
            $treeview,
            $listview
                ->addClass('workspace-vertical-auto')
				->setSizePolicy('filemanager-view-container')
        )->addClass('expand filemanager-explorer'),
        $uploadStatusBox
    );

    return $explorer;
}





/**
 * @param portletId
 */

function filemanager_configurationMenu($portletId)
{
    $W = bab_Widgets();

    $ctrlFile = filemanager_Controller()->File();

    $configurationMenu = $W->Menu(
        null,
        $W->VBoxLayout()
    );

    $displayTypeIcons = array(
        filemanager_CtrlFile::DISPLAY_ICONS => Func_Icons::ACTIONS_CONTEXT_MENU,
        filemanager_CtrlFile::DISPLAY_DETAILS => Func_Icons::ACTIONS_VIEW_LIST_DETAILS,
        filemanager_CtrlFile::DISPLAY_COMPACT => Func_Icons::ACTIONS_VIEW_LIST_TEXT,
        filemanager_CtrlFile::DISPLAY_THUMBNAIL => Func_Icons::ACTIONS_VIEW_LIST_CARDS,
    );


    $configurationMenu->setButtonClass('widget-actionbutton btn icon '. Func_Icons::ACTIONS_CONTEXT_MENU);
    $configurationMenu->setButtonLabel('');

    $displayIconsButton = $W->Link(
         filemanager_translate('Icon view'),
        $ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_ICONS)
    )->addClass('icon', $displayTypeIcons[filemanager_CtrlFile::DISPLAY_ICONS]);
    $displayIconsButton->setAjaxAction($ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_ICONS), $displayIconsButton);

    $configurationMenu->addItem($displayIconsButton);

    $displayDetailsButton = $W->Link(
        filemanager_translate('Details view'),
        $ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_DETAILS)
    )->addClass('icon', $displayTypeIcons[filemanager_CtrlFile::DISPLAY_DETAILS]);
    $displayDetailsButton->setAjaxAction($ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_DETAILS), $displayDetailsButton);

    $configurationMenu->addItem($displayDetailsButton);

    $displayCompactButton = $W->Link(
        filemanager_translate('Compact view'),
        $ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_COMPACT)
    )->addClass('icon', $displayTypeIcons[filemanager_CtrlFile::DISPLAY_COMPACT]);
    $displayCompactButton->setAjaxAction($ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_COMPACT), $displayCompactButton);

    $configurationMenu->addItem($displayCompactButton);

    $displayThumbnailButton = $W->Link(
        filemanager_translate('Thumbnail view'),
        $ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_THUMBNAIL)
    )->addClass('icon', $displayTypeIcons[filemanager_CtrlFile::DISPLAY_THUMBNAIL]);
    $displayThumbnailButton->setAjaxAction($ctrlFile->changeCurrentDisplayType($portletId, filemanager_CtrlFile::DISPLAY_THUMBNAIL), $displayThumbnailButton);

    $configurationMenu->addItem($displayThumbnailButton);


    $configurationMenu->addSeparator();

    $displayTinyButton = $W->Link(
        '',
        $ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_TINY)
    )->addClass('icon', Func_Icons::MIMETYPES_UNKNOWN, Func_Icons::ICON_LEFT_16)
    ->setTitle(filemanager_translate('Tiny'));
    $displayTinyButton->setAjaxAction($ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_TINY), $displayTinyButton);

    $displaySmallButton = $W->Link(
        '', //filemanager_translate('Small'),
        $ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_SMALL)
    )->addClass('icon', Func_Icons::MIMETYPES_UNKNOWN, Func_Icons::ICON_LEFT_24);
    $displaySmallButton->setAjaxAction($ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_SMALL), $displaySmallButton);

    $displayMediumButton = $W->Link(
        '', //filemanager_translate('Medium'),
        $ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_MEDIUM)
    )->addClass('icon', Func_Icons::MIMETYPES_UNKNOWN, Func_Icons::ICON_LEFT_32);
    $displayMediumButton->setAjaxAction($ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_MEDIUM), $displayMediumButton);

    $displayLargeButton = $W->Link(
        '', //filemanager_translate('Large'),
        $ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_LARGE)
    )->addClass('icon', Func_Icons::MIMETYPES_UNKNOWN, Func_Icons::ICON_LEFT_48);
    $displayLargeButton->setAjaxAction($ctrlFile->changeCurrentDisplayIconSize($portletId, filemanager_CtrlFile::DISPLAY_LARGE), $displayLargeButton);

    $configurationMenu->addItem(
        $W->HBoxItems(
            $W->Label('Zoom')->addClass('icon'),
            $displayTinyButton, //->setSizePolicy('widget-25pc'),
            $displaySmallButton, //->setSizePolicy('widget-25pc'),
            $displayMediumButton, //->setSizePolicy('widget-25pc'),
            $displayLargeButton //->setSizePolicy('widget-25pc')
        )->setVerticalAlign('middle')
    );

    $configurationMenu->addSeparator();

    $configurationMenu->addClass(Func_Icons::ICON_LEFT_16);

    $hideDirectoryTreeButton = $W->Link(
        filemanager_translate('Show side panel'),
        $ctrlFile->toggleDirectoryTreeVisibility($portletId)
    )->addClass('icon', filemanager_getConf($portletId . '_hideDirectoryTree') ? '' :Func_Icons::ACTIONS_DIALOG_OK);
    $hideDirectoryTreeButton->setAjaxAction($ctrlFile->toggleDirectoryTreeVisibility($portletId), $hideDirectoryTreeButton);

    $configurationMenu->addItem($hideDirectoryTreeButton);

    $hideDirectoryToolbarButton = $W->Link(
        filemanager_translate('Show toolbar'),
        $ctrlFile->toggleDirectoryToolbarVisibility($portletId)
    )->addClass('icon', filemanager_getConf($portletId . '_hideDirectoryToolbarButton') ? '' :Func_Icons::ACTIONS_DIALOG_OK);
    $hideDirectoryToolbarButton->setAjaxAction($ctrlFile->toggleDirectoryToolbarVisibility($portletId), $hideDirectoryToolbarButton);

    $configurationMenu->addItem($hideDirectoryToolbarButton);

    return $configurationMenu;
}



/**
 * @return Widget_Displayable_Interface
 */
function filemanager_filesSearchResultList($portletId, $path, $displayType = Func_Icons::ICON_TOP_48, $keywords = null, $userFolder)
{
    require_once $GLOBALS['babInstallPath'].'utilit/searchapi.php';
    require_once $GLOBALS['babInstallPath'].'utilit/searchincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/fileincl.php';

    $W = bab_Widgets();
    $App = filemanager_App();

    $ctrlFile = $App->Controller()->File();

    $rootPath = BAB_FileManagerEnv::getFmRealCollectivePath();


    switch ($displayType) {
        case filemanager_CtrlFile::DISPLAY_COMPACT:
            $iconClass = Func_Icons::ICON_LEFT_16;
            $thumbnailWidth = 14;
            $thumbnailHeight = 14;
            $layout = $W->FlowLayout(); //->setSpacing(4, 'px');
            $layout->setVerticalAlign('middle');
            break;
        case filemanager_CtrlFile::DISPLAY_ICONS:
        default:
            $iconClass = Func_Icons::ICON_TOP_32;
            $thumbnailWidth = 92;
            $thumbnailHeight = 22;
            $layout = $W->FlowLayout(); //->setSpacing(4, 'px');
            $layout->setVerticalAlign('top');
            break;
    }


    $realm = bab_Search::getRealm('bab_SearchRealmFiles');


    $results = array();

    $primaryCriteria = filemanager_searchStringToCriteria($realm->name, $keywords);

    if ($primaryCriteria) {

        // We define the search criteria on content and metadata.
        $realm->setFieldlessCriteria($primaryCriteria);

        // We define the search criteria on fields.
        $criteria = $realm->getDefaultCriteria();
        $criteria = $criteria->_AND_($primaryCriteria);


        foreach ($realm->search($criteria) as $record) {
            /* @var $record bab_SearchRecord */
            $results[$record->id] = array('record' => $record);
            foreach ($realm->getFields() as $field) {
                $fieldname = $field->getName();
                $results[$record->id][$fieldname] = array(
                    'name' => $field->getDescription(),
                    'value' => $record->$fieldname
                );
            }
        }
    }

    $pathElements = explode('/', $path);
    array_shift($pathElements);


    $fileBox = $W->FlowLayout()->setSpacing(4, 'px');

    $nbResults = 0;

    foreach ($results as $result) {

        if (strncmp('DG' . $result['record']->id_dgowner . '/' . $result['path']['value'], $path, strlen($path)) != 0) {
            continue;
        }
        $filePathname = 'DG' . $result['record']->id_dgowner . '/' . $result['path']['value'] . $result['name']['value'];


        $nbResults++;

        $file = new bab_FileInfo($rootPath . $filePathname);

        $fileIcon = filemanager_fileIcon($file, $displayType, $thumbnailWidth, $thumbnailHeight, $userFolder);

        $fileIcon->setMetadata('pathname', urlencode($file->getFmPathname()));

        $fileIcon->addClass('filemanager-draggable');
        $fileIcon->addClass('filemanager-icon');

        if ($file->isDir()) {
            $link = $W->Link(
                $fileIcon,
                $ctrlFile->browse($file->getFmPathname())
            );
            $link->setAjaxAction($ctrlFile->changeCurrentFolder($portletId, $file->getFmPathname()), $fileIcon);
        } else {
            $link = $W->Link(
                $fileIcon,
                $ctrlFile->displayFile($file->getFmPathname(), true, $userFolder)
            );
        }
        $resultRow = $W->VBoxItems(
            $fileIcon
         );

        $item = $W->FlowItems();
        $item->addClass('widget-actions-target');

        $contextMenu = filemanager_addContextMenu($file, $fileIcon);
        $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
        if (!$menuEmpty) {
            $item->addItem(
                $contextMenu->setSizePolicy('widget-actions')
            );
        }

        $item->addItem($resultRow);

        $fileBox->addItem($item);
    }

    $resultClass = 'alert alert-success';
    if ($nbResults == 0) {
        $resultClass = 'alert alert-danger';
    }
    $resultBox = $W->VBoxItems(
        $W->Html(
            sprintf($App->translate('<b>%d</b> search result for <b>%s</b> in <b>%s</b>', '<b>%d</b> search results for <b>%s</b> in <b>%s</b>', $nbResults), $nbResults, $keywords, implode('/', $pathElements))
        )->addClass($resultClass),
        $fileBox
    )->setVerticalSpacing(16, 'px');

    $listView = $W->Frame()
        ->setLayout($resultBox)
        ->addClass('widget-list-view ' . $iconClass);

    return $listView;
}

function filemanager_fileHistory(filemanager_File $file)
{
    require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

    $W = bab_Widgets();
    $mainTable = $W->TableArrayView();
    $ctrlFile = filemanager_Controller()->File();
    $pathname = $file->getFmPathname();
    $userFolder = $file->isPersonal();

    $content = array();
    $header = array(
        filemanager_translate('Version'),
        filemanager_translate('Date'),
        filemanager_translate('Time'),
        filemanager_translate('Author'),
        filemanager_translate('Comment')
    );

    $fileDate = BAB_DateTime::fromIsoDateTime($file->getModifiedDate());

    $current = array(
        'version' => $file->getVersion(),
        'date' => $fileDate->getIsoDate(),
        'time' => $fileDate->getIsoTime(),
        'author' => $file->getAuthorName(),
        'comment' => ''
    );

    $hasRightToDownload = false;
    if (filemanager_canDownloadFileFromFolder(str_replace('TR', '', $file->getFmPath(false)), $userFolder)) {
        $hasRightToDownload = true; //We set up this flag only once, has it is not necessary to redo the process of checking the right for each version
        $current['version'] = $W->Link(
            $file->getVersion(),
            $ctrlFile->displayFile($pathname, false, $userFolder)
        );
    }

    $content[] = $current;

    $allVersions = $file->getAllVersions();
    foreach ($allVersions as $version){
        /* @var $version filemanager_FileVersion */
        $versionDate = BAB_DateTime::fromIsoDateTime($version->date);
        $link = $W->Link(
            $version->getVersion(),
            $ctrlFile->displayVersionFile($version->id)
        );

        $content[] = array(
            'version' => $hasRightToDownload ? $link : $version->getVersion(),
            'date' => $versionDate->getIsoDate(),
            'time' => $versionDate->getIsoTime(),
            'author' => $version->getAuthorName(),
            'comment' => $version->comment
        );

    }

    $mainTable->setHeader($header);
    $mainTable->setContent($content);

    return $mainTable;
}
