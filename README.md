## Gestionnaire de fichiers ##

This addon provides an easy to use portlet for managing folders of the Ovidentia filemanager:

*   Integrates in any page with portlet containers or any article,
*   Proposes several views (large or small icons, detailed, image gallery...),
*   Drag and drop file upload