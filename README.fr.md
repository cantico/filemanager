Ce module fournit un portlet qui permet une gestion simplifiée du contenu des répertoires du gestionnaire de fichiers d'Ovidentia:

*   S'intègre dans n'importe quelle page ayant des zones de portlets containers ou n'importe quel article,
*   Propose plusieurs modes d'affichage (grande ou petites icônes, vue détaillée, gallerie d'images...),
*   Permet de déposer des fichiers par simple "glisser-déposer"